/**
 * CSCI 205 - Software Design and Engineering
 * Name(s): Seline Tan-Torres
 *
 * Work: Final Project
 * Created: Nov 19, 2014, 12:46:45 AM
 */
package theView;

import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * @author satt001
 *
 */
public class BuildPanel extends JPanel {
	/**
	 * roadBtn allows you to build a road
	 */
	private JButton roadBtn = new JButton("Build Road");
	/**
	 * cityBtn allows you to build a city
	 */
	private JButton cityBtn = new JButton("Build City");
	/**
	 * settleBtn allows you to build a settlement
	 */
	private JButton settleBtn = new JButton("Build Settlement");
	/**
	 * devBtn allows you to buy a development card
	 */
	private JButton devBtn = new JButton("Buy Development Card");
	/**
	 * seeDevCards allows you to see your development cards
	 */
	private JButton seeDevCards = new JButton("See Development Cards");
	private JPanel developmentCards = new JPanel();

	/**
	 * constructs the build panel
	 */
	public BuildPanel() {
		this.setLayout(new GridLayout(5, 1, 5, 5));
		JLabel buildLbl = new JLabel("Build");
		buildLbl.setHorizontalAlignment(SwingConstants.CENTER);
		buildLbl.setFont(new Font("Serif", Font.PLAIN, 30));
		this.add(buildLbl);
		this.add(roadBtn);
		this.add(cityBtn);
		this.add(settleBtn);
		this.developmentCards.add(devBtn);
		this.developmentCards.add(seeDevCards);
		this.add(developmentCards);

	}

	/**
	 * @return the roadBtn
	 */
	public JButton getRoadBtn() {
		return roadBtn;
	}

	/**
	 * @return the cityBtn
	 */
	public JButton getCityBtn() {
		return cityBtn;
	}

	/**
	 * @return the settleBtn
	 */
	public JButton getSettleBtn() {
		return settleBtn;
	}

	/**
	 * @return the devBtn
	 */
	public JButton getDevBtn() {
		return devBtn;
	}

	/**
	 * @return the devBtn
	 */
	public JButton getSeeDevelopmentCards() {
		return seeDevCards;
	}
}
