package theView;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;

import javax.swing.JLabel;

public class HexLayout implements LayoutManager {

	/** the columns in the layout */
	private int columns;
	/** the rows in the layout */
	private int rows;

	public HexLayout(int rows, int columns) {
		this.columns = columns;
		this.rows = rows;
	}

	@Override
	public void addLayoutComponent(String name, Component comp) {
	}

	@Override
	public void layoutContainer(Container parent) {
		int hexHeight = (Math.min(parent.getWidth(), parent.getHeight()) / (this.rows + 1)) * 2;
		int hexWidth = hexHeight;

		int offCenterX = (parent.getWidth() - 9 * hexWidth / 2) / 2;
		int offCenterY = (parent.getHeight() - 5 * hexHeight) / 2;
		int currentRow = 0, currentColumn = 0, xOffset = 0, yOffset = 0, iteration = 1, cornerButtons = 0, portNum = 0;

		boolean firstHexButton = true, firstRoad = true, reset = true;
		int roadLength = hexWidth / 3;
		for (Component component : parent.getComponents()) {
			if (component instanceof JLabel) {
				yOffset = (int) (.9 * currentRow * (hexHeight / 2)) - 10
						* hexHeight / 100;
				if (iteration % 2 == 0) {
					xOffset = (int) (hexWidth * 0.75) + currentColumn
							* hexWidth + currentColumn * (hexWidth / 2);
					yOffset += hexHeight / 2;
				} else {
					xOffset = currentColumn * hexWidth + currentColumn
							* (hexWidth / 2);
					yOffset += hexHeight / 2;
				}
				try {
					if (Integer.parseInt(((JLabel) component).getText()) > 9) {
						xOffset -= 3 * hexHeight / 100;
					}
				} catch (NumberFormatException e) {

				}
				xOffset += offCenterX - 5 * hexHeight / 100;
				yOffset += offCenterY;
				component.setBounds(xOffset + hexWidth / 10 + hexWidth / 2,
						yOffset, 40, 40);
				currentColumn++;
				cornerButtons = ((cornerButtons + 1) % 6);
				if ((iteration % 2 == 0 && currentColumn == this.columns - 1)
						|| (iteration % 2 == 1 && currentColumn == this.columns)) {
					currentColumn = 0;
					currentRow++;
					iteration++;
				}
				continue;
			} else if (reset) {
				reset = false;
				currentRow = 0;
				currentColumn = 0;
				iteration = 1;
				cornerButtons = 0;
			}
			if (component instanceof Port) {
				if (portNum % 3 == 0) {
					component
							.setBounds(
									parent.getWidth()
											/ 2
											- offCenterX
											+ (int) (2.5 * hexHeight * Math
													.cos((portNum * 2 * Math.PI / 9)
															+ Math.PI / 2)),
									parent.getHeight()
											/ 2
											- offCenterY
											/ 3
											- (int) ((parent.getHeight() / 2 - 7 * hexHeight / 15) * Math
													.sin((portNum * 2 * Math.PI / 9)
															+ Math.PI / 2)),
									30, 30);
				} else {

					component
							.setBounds(
									parent.getWidth()
											/ 2
											- offCenterX
											+ (int) (2.2 * hexHeight * Math
													.cos((portNum * 2 * Math.PI / 9)
															+ Math.PI / 2)),
									parent.getHeight()
											/ 2
											- 2
											* offCenterY
											/ 7
											- (int) ((parent.getHeight() / 2 - 5 * hexHeight / 6) * Math
													.sin((portNum * 2 * Math.PI / 9)
															+ Math.PI / 2)),
									30, 30);
				}
				portNum++;
				continue;
			}
			if (component instanceof HexButton) {
				if (firstHexButton) {
					currentRow = 0;
					currentColumn = 0;
					iteration = 1;
					cornerButtons = 0;
					firstHexButton = false;
				}
				if (iteration % 2 == 0) {
					xOffset = (int) (hexWidth * 0.75) + currentColumn
							* hexWidth + currentColumn * (hexWidth / 2);
					yOffset = currentRow
							* (hexHeight / 2 - 6 * hexHeight / 100);
				} else {
					xOffset = currentColumn * hexWidth + currentColumn
							* (hexWidth / 2);
					yOffset = currentRow
							* (hexHeight / 2 - 6 * hexHeight / 100);
				}
				xOffset += offCenterX;
				yOffset += offCenterY;
				component.setBounds(xOffset + hexWidth / 10, yOffset
						+ hexHeight / 10, hexWidth, hexHeight);
				currentColumn++;
			} else {
				if (component instanceof Road && firstRoad) {
					currentRow = 0;
					currentColumn = 0;
					iteration = 1;
					cornerButtons = 0;
					firstRoad = false;
				}
				if (cornerButtons == 0) {
					if (iteration % 2 == 0) {
						xOffset = (int) (hexWidth * 0.75) + currentColumn
								* hexWidth + currentColumn * (hexWidth / 2);
						yOffset = currentRow
								* (hexHeight / 2 - 6 * hexHeight / 100);
					} else {
						xOffset = currentColumn * hexWidth + currentColumn
								* (hexWidth / 2);
						yOffset = currentRow
								* (hexHeight / 2 - 6 * hexHeight / 100);
					}
					xOffset += offCenterX;
					yOffset += offCenterY;
					currentColumn++;
				}
				int x = hexWidth
						/ 2
						+ (int) ((hexWidth - 2 * hexHeight / 100) / 2 * Math
								.cos((cornerButtons + 1) * 2 * Math.PI / 6));
				int y = hexHeight
						/ 2
						+ (int) ((hexHeight - 2 * hexHeight / 100) / 2 * Math
								.sin((cornerButtons + 1) * 2 * Math.PI / 6));
				if (!(component instanceof Road))
					component.setBounds(x + xOffset + hexWidth / 20, y
							+ yOffset + hexHeight / 25, (int) (hexWidth * .15),
							(int) (hexWidth * .15));
				else if (component.isVisible()) {
					if (cornerButtons == 0)
						component.setBounds((int) ((.6 * x)) + xOffset, y
								+ yOffset - hexHeight / 20, roadLength,
								roadLength);
					if (cornerButtons == 1) {
						component.setBounds((int) (.6 * x) + xOffset - hexWidth
								/ 20, y + yOffset - hexHeight / 4, roadLength,
								roadLength);
					}
					if (cornerButtons == 2) {
						component.setBounds((int) (.75 * x) + hexWidth / 10
								+ xOffset, y + yOffset - 7 * hexHeight / 24,
								roadLength, roadLength);
					}
					if (cornerButtons == 3) {
						component.setBounds((int) (2.7 * x) + xOffset - x, y
								+ yOffset - hexHeight / 20, roadLength,
								roadLength);
					}
					if (cornerButtons == 4) {
						component.setBounds(x + xOffset
								+ (int) (hexWidth * .08), y + yOffset
								+ (int) (hexWidth * .15), roadLength,
								roadLength);
					}
					if (cornerButtons == 5) {
						component.setBounds(x + xOffset
								- (int) (hexWidth * .22), y + yOffset
								+ hexHeight / 6, roadLength, roadLength);
					}
				}

			}
			cornerButtons = ((cornerButtons + 1) % 6);
			if ((iteration % 2 == 0 && currentColumn == this.columns - 1)
					|| (iteration % 2 == 1 && currentColumn == this.columns)) {
				currentColumn = 0;
				currentRow++;
				iteration++;
			}

		}

	}

	@Override
	public Dimension minimumLayoutSize(Container parent) {
		return new Dimension(600, 500);
	}

	@Override
	public Dimension preferredLayoutSize(Container parent) {
		return new Dimension(600, 500);
	}

	@Override
	public void removeLayoutComponent(Component comp) {
		// TODO Auto-generated method stub
	}

}
