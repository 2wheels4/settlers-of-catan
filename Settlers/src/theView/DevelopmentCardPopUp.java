package theView;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import model.Card;
import model.Player;

/**
 * CSCI 205 - Software Design and Engineering
 * Name(s): Cole Conte
 * 
 * Work:	Settlers
 * Created:	Dec 8, 2014
 */

/**
 * @author coleconte
 *
 */
public class DevelopmentCardPopUp extends JFrame {
	/**
	 * chapel is the button for the chapel dev card
	 */
	private JButton chapel = new JButton();
	/**
	 * governorsHouse is the button for the governor's house dev card
	 */
	private JButton governorsHouse = new JButton();
	/**
	 * library is the button for the library dev card
	 */
	private JButton library = new JButton();
	/**
	 * market is the button for the market dev card
	 */
	private JButton market = new JButton();
	/**
	 * monopoly is the button for the monopoly dev card
	 */
	private JButton monopoly = new JButton();
	/**
	 * roadBuilding is the button for the road building dev card
	 */
	private JButton roadBuilding = new JButton();
	/**
	 * soldier is the button for the soldier dev card
	 */
	private JButton soldier = new JButton();
	/**
	 * universityOfCatan is the button for the university dev card
	 */
	private JButton universityOfCatan = new JButton();
	/**
	 * yearOfPlenty is the button for the year of plenty dev card
	 */
	private JButton yearOfPlenty = new JButton();
	/**
	 * cancel is the button to exit the pop up
	 */
	private JButton cancel = new JButton("Cancel");

	/**
	 * construct the pop up for a player
	 * 
	 * @param player
	 *            - the player to construct the pop up for
	 */
	public DevelopmentCardPopUp(Player player) {
		this.setTitle("Use Development Cards");
		setLocationRelativeTo(null);
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(3, 3));
		chapel.setIcon(new ImageIcon("src/theView/chapel.png"));
		if (player.getDevCards().contains(Card.CHAPEL)) {
			chapel.setEnabled(true);
		} else {
			chapel.setEnabled(false);
		}
		panel.add(chapel);
		governorsHouse.setIcon(new ImageIcon("src/theView/governorsHouse.png"));
		if (player.getDevCards().contains(Card.GOVERNORSHOUSE)) {
			governorsHouse.setEnabled(true);
		} else {
			governorsHouse.setEnabled(false);
		}
		panel.add(governorsHouse);
		library.setIcon(new ImageIcon("src/theView/library.png"));
		if (player.getDevCards().contains(Card.LIBRARY)) {
			library.setEnabled(true);
		} else {
			library.setEnabled(false);
		}
		panel.add(library);
		market.setIcon(new ImageIcon("src/theView/market.png"));
		if (player.getDevCards().contains(Card.MARKET)) {
			market.setEnabled(true);
		} else {
			market.setEnabled(false);
		}
		panel.add(market);
		monopoly.setIcon(new ImageIcon("src/theView/monopoly.png"));
		if (player.getDevCards().contains(Card.MONOPOLY)) {
			monopoly.setEnabled(true);
		} else {
			monopoly.setEnabled(false);
		}
		panel.add(monopoly);
		roadBuilding.setIcon(new ImageIcon("src/theView/roadBuilding.png"));
		if (player.getDevCards().contains(Card.ROADBUILDING)) {
			roadBuilding.setEnabled(true);
		} else {
			roadBuilding.setEnabled(false);
		}
		panel.add(roadBuilding);
		soldier.setIcon(new ImageIcon("src/theView/soldier.png"));
		if (player.getDevCards().contains(Card.SOLDIER)) {
			soldier.setEnabled(true);
		} else {
			soldier.setEnabled(false);
		}
		panel.add(soldier);
		universityOfCatan.setIcon(new ImageIcon(
				"src/theView/universityOfCatan.png"));
		if (player.getDevCards().contains(Card.UNIVERSITYOFCATAN)) {
			universityOfCatan.setEnabled(true);
		} else {
			universityOfCatan.setEnabled(false);
		}
		panel.add(universityOfCatan);
		yearOfPlenty.setIcon(new ImageIcon("src/theView/yearOfPlenty.png"));
		if (player.getDevCards().contains(Card.YEAROFPLENTY)) {
			yearOfPlenty.setEnabled(true);
		} else {
			yearOfPlenty.setEnabled(false);
		}
		panel.add(yearOfPlenty);
		this.add(panel, BorderLayout.NORTH);
		JPanel panel2 = new JPanel();
		panel2.setLayout(new GridLayout(1, 1));
		panel2.add(cancel);
		this.add(panel2, BorderLayout.SOUTH);
		this.pack();
		this.setVisible(true);
	}

	/**
	 * @return the chapel
	 */
	public JButton getChapel() {
		return chapel;
	}

	/**
	 * @return the governorsHouse
	 */
	public JButton getGovernorsHouse() {
		return governorsHouse;
	}

	/**
	 * @return the library
	 */
	public JButton getLibrary() {
		return library;
	}

	/**
	 * @return the market
	 */
	public JButton getMarket() {
		return market;
	}

	/**
	 * @return the monopoly
	 */
	public JButton getMonopoly() {
		return monopoly;
	}

	/**
	 * @return the roadBuilding
	 */
	public JButton getRoadBuilding() {
		return roadBuilding;
	}

	/**
	 * @return the soldier
	 */
	public JButton getSoldier() {
		return soldier;
	}

	/**
	 * @return the universityOfCatan
	 */
	public JButton getUniversityOfCatan() {
		return universityOfCatan;
	}

	/**
	 * @return the yearOfPlenty
	 */
	public JButton getYearOfPlenty() {
		return yearOfPlenty;
	}

	/**
	 * @return the cancel
	 */
	public JButton getCancel() {
		return cancel;
	}
}
