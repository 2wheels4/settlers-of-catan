package theView;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import model.Card;

public class HexButton extends JButton {

	/** myBounds is the polygon representing the button image */
	private Polygon myBounds;
	/** this is true if the hexbutton has the robber on it */
	private Boolean hasRobber;
	/** this hold the color of hexbutton's inner circle */
	private Color myColour;
	/** this is the hexagons number which is used when the die are rolled */
	private int myNumber;

	private String myResource = "";

	/** makes a new button with a hexagon as its background */
	public HexButton() {
		this.calculateBounds();
		this.setBackground(Color.BLUE);
		this.hasRobber = false;
		this.setBorderPainted(false);
		this.myColour = Color.WHITE;
	}

	public void setResource(String resource) {
		this.myResource = resource;
	}

	public String getResource() {
		return this.myResource;
	}

	public void setMyNumber(int myNumber) {
		this.myNumber = myNumber;
	}

	public int getMyNumber() {
		return this.myNumber;
	}

	public HexButton(String text) {
		this();
		this.setText(text);
	}

	/**
	 * makes a polygon
	 * 
	 * @param width
	 *            the width of the hexagon
	 * @param height
	 *            the height of the hexagon
	 * @param ratio
	 *            a scaling factor
	 * @return the polygon for the hexagon
	 */
	private Polygon hexagon(int width, int height, double ratio) {
		Polygon hexagon = new Polygon();
		for (int i = 0; i < 6; i++) {
			int x = width
					/ 2
					+ (int) ((width - 2) / 2 * Math.cos(i * 2 * Math.PI / 6) * ratio);
			int y = height
					/ 2
					+ (int) ((height - 2) / 2 * Math.sin(i * 2 * Math.PI / 6) * ratio);
			hexagon.addPoint(x, y);
		}
		return hexagon;
	}

	private void calculateBounds() {
		this.myBounds = this.hexagon(this.getWidth(), this.getHeight(), 1.0);
	}

	@Override
	public boolean contains(Point p) {
		return this.myBounds.contains(p);
	}

	@Override
	public boolean contains(int x, int y) {
		return this.myBounds.contains(x, y);
	}

	@Override
	public void setSize(Dimension d) {
		super.setSize(d);
		this.calculateBounds();
	}

	@Override
	public void setSize(int w, int h) {
		super.setSize(w, h);
		this.calculateBounds();
	}

	@Override
	public void setBounds(int x, int y, int width, int height) {
		super.setBounds(x, y, width, height);
		this.calculateBounds();
	}

	@Override
	public void setBounds(Rectangle r) {
		super.setBounds(r);
		this.calculateBounds();
	}

	/**
	 * changes the color of the inner circle of the hexbutton
	 */
	public void updateColor() {
		if (this.myColour.equals(Color.WHITE)) {
			this.myColour = Color.BLACK;
			this.hasRobber = true;
		} else {
			this.myColour = Color.WHITE;
			this.hasRobber = false;
		}
	}

	@Override
	protected void paintComponent(Graphics graphics) {
		Graphics2D g2 = (Graphics2D) graphics;
		g2.setClip(this.hexagon(getWidth(), getHeight(), 1.02));
		if (this.getResource().equals("")) {
			this.setResource(Card.DESERT.getTile());
		}
		ImageIcon image = new ImageIcon(this.myResource);
		image.setImageObserver(this);
		g2.drawImage(image.getImage(), 0, 0, getWidth(), getHeight(), this);
		graphics.setColor(this.myColour);
		graphics.fillOval(getWidth() * 2 / 5, getHeight() * 2 / 5,
				getWidth() / 5, getWidth() / 5);
	}

	/**
	 * @return true if the hexbutton has the robber on it
	 */
	public boolean hasRobber() {
		return this.hasRobber;
	}

	public void setRobber(boolean hasRobber) {
		this.hasRobber = hasRobber;

	}
}