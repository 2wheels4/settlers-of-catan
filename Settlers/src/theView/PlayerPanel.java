package theView;

import java.awt.GridLayout;
import java.io.IOException;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import model.Board;
import model.Player;

/**
 * CSCI 205 - Software Design and Engineering
 * Name(s): Cole Conte
 * 
 * Work:	FinalProject
 * Created:	Nov 19, 2014
 */

/**
 * @author coleconte
 *
 */
public class PlayerPanel extends JPanel {
	/**
	 * pointTotal is the total number of points the player has
	 */
	JLabel pointTotal;
	/**
	 * playerStats is a 2d array of jlabels
	 */
	JLabel[][] playerStats;
	/**
	 * theBoard is the board related to the player panel
	 */
	Board theBoard;

	/**
	 * constructor for the player panel
	 * 
	 * @param b
	 *            - the board related to the player panel
	 * @throws IOException
	 */
	public PlayerPanel(Board b) throws IOException {
		this.theBoard = b;
		this.playerStats = new JLabel[theBoard.getPlayers().size()][6];
		this.setLayout(new GridLayout(this.theBoard.getPlayers().size() + 1, 6));
		this.add(new JLabel(""));
		this.add(new JLabel("Resources", SwingConstants.CENTER));
		this.add(new JLabel("Dev Cards", SwingConstants.CENTER));
		this.add(new JLabel("Knights", SwingConstants.CENTER));
		this.add(new JLabel("Long Road", SwingConstants.CENTER));
		this.add(new JLabel("Points", SwingConstants.CENTER));
		int i = 0;

		for (Player p : theBoard.getPlayers()) {
			playerStats[i][0] = new JLabel("");
			playerStats[i][1] = new JLabel(String.valueOf(p.getMyHand().size()));
			playerStats[i][2] = new JLabel(String.valueOf(p.getDevCards()
					.size()));
			playerStats[i][3] = new JLabel(String.valueOf(p.getKnights()));
			playerStats[i][4] = new JLabel(String.valueOf(p.getLongestRoad()));
			playerStats[i][5] = new JLabel(String.valueOf(p.getVPcount()));
			for (int j = 0; j < 6; j++) {
				playerStats[i][j].setBackground(p.getMyColor());
				playerStats[i][j].setOpaque(true);
				playerStats[i][j].setHorizontalAlignment(SwingConstants.CENTER);
				this.add(playerStats[i][j]);
			}
			i++;
		}

	}

	/**
	 * after any move, updates the stats of a player
	 * 
	 * @param player
	 *            - the player to update stats for
	 */

	public boolean updateStats(Player player) {
		int i = 0, totalCards = 0, longestRoad = 0, biggestArmy = 0, addKnightsVp = 0, addLongRoad = 0;
		for (Player p : theBoard.getPlayers()) {
			if (p.equals(player)) {
				break;
			}
			i++;
		}
		for (Player players : theBoard.getPlayers()) {
			if (players.getLongestRoad() > longestRoad) {
				longestRoad = players.getLongestRoad();
			}
			if (players.getKnights() > biggestArmy) {
				biggestArmy = players.getKnights();
			}
		}
		if (player.getLongestRoad() == longestRoad && longestRoad > 4) {
			addLongRoad = 2;
		}
		if (player.getKnights() == biggestArmy && biggestArmy > 2) {
			addKnightsVp = 2;
		}
		for (int index = 0; index < 6; index++) {
			totalCards += player.getMyHand().get(index);
		}
		if (player.equals(theBoard.getCurrentPlayer())) {
			playerStats[i][0].setText("->");
		} else {
			playerStats[i][0].setText("");
		}
		playerStats[i][1].setText(totalCards + "");
		playerStats[i][2].setText(String.valueOf(player.getDevCards().size()));
		playerStats[i][3].setText(String.valueOf(player.getKnights()));
		playerStats[i][4].setText(String.valueOf(player.getLongestRoad()));
		playerStats[i][5].setText(String.valueOf(player.getVPcount()
				+ addKnightsVp + addLongRoad));
		return (player.getVPcount() + addKnightsVp + addLongRoad > 9);
	}

}
