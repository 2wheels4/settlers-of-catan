package theView;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;

import model.Board;

public class MainView extends JFrame {

	/** the panel holding the board graphic */
	private BoardPanel boardPanel;
	/** the panel displaying the users hand as a graphic */
	private HandPanel handPanel;
	/** the panel holding the player's options as a graphic */
	private OptionsPanel optionsPanel;
	/**
	 * the panel holding the information about the player's information as a
	 * graphic
	 */
	private PlayerPanel playerPanel;
	/** the panel holding the build options as a graphic */
	private BuildPanel buildPanel;

	/** creates a jframe that combines all of the panels above */
	public MainView(Board board) throws IOException {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Settlers of Catan");
		setLocationRelativeTo(null);
		this.boardPanel = new BoardPanel();
		this.add(this.boardPanel, BorderLayout.CENTER);
		JPanel rightPanel = new JPanel();
		rightPanel.setMaximumSize(new Dimension(100, 100));
		rightPanel.setLayout(new GridLayout(4, 1));
		this.optionsPanel = new OptionsPanel();
		rightPanel.add(optionsPanel);
		this.handPanel = new HandPanel();
		rightPanel.add(this.handPanel);
		this.playerPanel = new PlayerPanel(board);
		rightPanel.add(playerPanel);
		this.buildPanel = new BuildPanel();
		rightPanel.add(buildPanel);
		this.add(rightPanel, BorderLayout.EAST);
		this.pack();
		this.setVisible(true);
		this.boardPanel.hideRoads();
	}

	/**
	 * @return the bPanel
	 */
	public BoardPanel getBoardPanel() {
		return boardPanel;
	}

	/**
	 * @return the pPanel
	 */
	public PlayerPanel getPlayerPanel() {
		return this.playerPanel;
	}

	/**
	 * @return the hPanel
	 */
	public HandPanel getHandPanel() {
		return handPanel;
	}

	/**
	 * @return the buildPanel
	 */
	public BuildPanel getBuildPanel() {
		return buildPanel;
	}

	/**
	 * @return the oPanel
	 */
	public OptionsPanel getOptionsPanel() {
		return optionsPanel;
	}

}
