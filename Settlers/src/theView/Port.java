/**
 * CSCI 205 - Software Design and Engineering
 * Name(s): Andrew Wheeler
 *  
 *  Work:	Settlers
 *  Created Dec 4, 2014, 11:25:45 AM
 */
package theView;

import java.awt.Graphics;

import javax.swing.JButton;

import model.Card;

/**
 * @author asw011
 *
 */
public class Port extends JButton {

	/** the resource being traded at the port */
	private Card myResource;
	/** the center position of the port */
	private int[] position;

	public Port(Card myResource) {
		this.setBorderPainted(false);
		this.setOpaque(true);
		this.myResource = myResource;
		this.position = new int[2];
		this.position[0] = this.getX() + this.getWidth() / 2;
		this.position[1] = this.getY() + this.getHeight() / 2;
	}

	public int[] getPosition() {
		return this.position;
	}

	public void setPosition(int[] position) {
		this.position = position;
	}

	public Card getResource() {
		return this.myResource;
	}

	@Override
	protected void paintComponent(Graphics graphics) {
		super.paintComponent(graphics);

	}
}
