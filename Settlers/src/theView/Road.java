package theView;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;

import javax.swing.JButton;

public class Road extends JButton {

	/** the angle of rotation needed for the road's paint component method */
	private double angle = 0;
	/** the color of the road */
	private Color myColor;

	private ArrayList<Road> neighbors = new ArrayList<Road> ();

	public Road(int angle) {
		this.angle = Math.PI * 2 * angle / 6;
	}

	/** this method paints the road and rotates it about its center */
	public void paint(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		g2.rotate(this.angle, getWidth() / 2.0, getHeight() / 2.0);
		double diagonal = Math.sqrt(getWidth() * getWidth() + getHeight()
				* getHeight());
		double scale = Math.min(getWidth(), getHeight()) / diagonal;
		g2.translate(getWidth() / 2.0, getHeight() / 2.0);
		g2.scale(scale, scale / 3);
		g2.translate(-getWidth() / 2.0, -getHeight() / 2.0);
		super.paint(g);
	}
	
	public void addNeighbor(Road road){
		this.neighbors.add(road);
	}
	
	public ArrayList<Road> getNeighbors(){
		return this.neighbors;
	}

	public double getAngle() {
		return angle;
	}

	public void setColor(Color color) {
		this.myColor = color;
	}

	public Color getColor() {
		return this.myColor;
	}

	public void setAngle(int angle) {
		this.angle = Math.PI * 2 * angle / 6;
		repaint();
	}

	public Dimension getPreferredSize() {
		Dimension d = new Dimension(getWidth(), getHeight());
		return d;
	}
}
