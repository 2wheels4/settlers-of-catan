/**
 * CSCI 205 - Software Design and Engineering
 * Name(s): Seline Tan-Torres
 *
 * Work: Final Project
 * Created: Nov 19, 2014, 1:01:55 AM
 */
package theView;

import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * @author satt001
 *
 */
public class OptionsPanel extends JPanel {
	/**
	 * info tells the player what to do
	 */
	private JLabel info = new JLabel("Place your settlement");
	/**
	 * trade is a jbutton that when clicked allows a user to trade with the bank
	 */
	private JButton tradeBtn = new JButton("Trade");
	/**
	 * endTurn is a jbutton that when clicked ends a player's turn
	 */
	private JButton endTurnBtn = new JButton("End Turn");

	/**
	 * constructs the options panels
	 */
	public OptionsPanel() {
		this.setLayout(new GridLayout(4, 1, 5, 5));
		JLabel optionsLbl = new JLabel("Options");
		optionsLbl.setHorizontalAlignment(SwingConstants.CENTER);
		optionsLbl.setFont(new Font("Serif", Font.PLAIN, 30));
		this.add(optionsLbl);
		this.add(info);
		this.add(tradeBtn);
		this.add(endTurnBtn);
	}

	/**
	 * @return the rollBtn
	 */
	public JLabel getInfoLabel() {
		return info;
	}

	/**
	 * @return the tradeBtn
	 */
	public JButton getTradeBtn() {
		return tradeBtn;
	}

	/**
	 * @return the endTurnBtn
	 */
	public JButton getEndTurnBtn() {
		return endTurnBtn;
	}

}
