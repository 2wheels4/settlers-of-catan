/**
 * CSCI 205 - Software Design and Engineering
 * Name(s): Seline Tan-Torres
 *
 * Work: Final Project
 * Created: Nov 18, 2014, 9:34:07 PM
 */
package theView;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * @author satt001
 *
 */
public class HandPanel extends JPanel {

	/**
	 * brick is a jbutton representing the brick resource
	 */
	private JButton brick = new JButton();
	/**
	 * wood is a jbutton representing the wood resource
	 */
	private JButton wood = new JButton();
	/**
	 * ore is a jbutton representing the ore resource
	 */
	private JButton ore = new JButton();
	/**
	 * sheep is a jbutton representing the sheep resource
	 */
	private JButton sheep = new JButton();
	/**
	 * wheat is a jbutton representing the wheat resource
	 */
	private JButton wheat = new JButton();
	/**
	 * wheatCount is a jlabel representing the number of wheats a player has
	 */
	private JLabel wheatCount = new JLabel();
	/**
	 * brickCount is a jlabel representing the number of bricks a player has
	 */
	private JLabel brickCount = new JLabel();
	/**
	 * sheepCount is a jlabel representing the number of sheep a player has
	 */
	private JLabel sheepCount = new JLabel();
	/**
	 * oreCount is a jlabel representing the number of ores a player has
	 */
	private JLabel oreCount = new JLabel();
	/**
	 * woodCount is a jlabel representing the number of woods a player has
	 */
	private JLabel woodCount = new JLabel();
	/**
	 * vpCount is a jlabel representing the number of victory points a player
	 * has
	 */
	private JLabel vpCount = new JLabel();
	/**
	 * cardPanels is an array list to hold all the cards
	 */
	private ArrayList<JButton> cardPanels = new ArrayList<JButton>();

	/**
	 * constructs the hand panel
	 * 
	 * @throws IOException
	 */
	public HandPanel() throws IOException {
		JLabel resources = new JLabel();
		this.setLayout(new GridLayout(2, 1));
		resources.setLayout(new GridLayout(1, 4, 5, 5));
		brick.setIcon(new ImageIcon("src/theView/brick.png"));
		wood.setIcon(new ImageIcon("src/theView/wood.png"));
		ore.setIcon(new ImageIcon("src/theView/ore.png"));
		sheep.setIcon(new ImageIcon("src/theView/sheep.png"));
		wheat.setIcon(new ImageIcon("src/theView/wheat.png"));

		resources.add(brick);
		resources.add(wood);
		resources.add(ore);
		resources.add(sheep);
		resources.add(wheat);

		this.add(resources, BorderLayout.CENTER);

		JLabel handCount = new JLabel();
		handCount.setLayout(new GridLayout(1, 4, 5, 5));

		brickCount.setHorizontalAlignment(SwingConstants.CENTER);
		brickCount.setText("0");
		brickCount.setFont(new Font("Serif", Font.PLAIN, 30));

		woodCount.setHorizontalAlignment(SwingConstants.CENTER);
		woodCount.setFont(new Font("Serif", Font.PLAIN, 30));
		woodCount.setText("0");

		oreCount.setHorizontalAlignment(SwingConstants.CENTER);
		oreCount.setText("0");
		oreCount.setFont(new Font("Serif", Font.PLAIN, 30));

		sheepCount.setHorizontalAlignment(SwingConstants.CENTER);
		sheepCount.setText("0");
		sheepCount.setFont(new Font("Serif", Font.PLAIN, 30));

		wheatCount.setHorizontalAlignment(SwingConstants.CENTER);
		wheatCount.setText("0");
		wheatCount.setFont(new Font("Serif", Font.PLAIN, 30));
		handCount.add(brickCount);
		handCount.add(woodCount);
		handCount.add(oreCount);
		handCount.add(sheepCount);
		handCount.add(wheatCount);
		cardPanels.add(sheep);
		cardPanels.add(wheat);
		cardPanels.add(brick);
		cardPanels.add(wood);
		cardPanels.add(ore);
		this.add(handCount, BorderLayout.SOUTH);
	}

	public ArrayList<JButton> getHandPanelButtons() {
		return this.cardPanels;
	}

	/**
	 * @return the brick
	 */
	public JButton getBrick() {
		return brick;
	}

	/**
	 * @return the wood
	 */
	public JButton getWood() {
		return wood;
	}

	/**
	 * @return the ore
	 */
	public JButton getOre() {
		return ore;
	}

	/**
	 * @return the sheep
	 */
	public JButton getSheep() {
		return sheep;
	}

	/**
	 * @return the wheat
	 */
	public JButton getWheat() {
		return wheat;
	}

	/**
	 * @param string
	 * @return
	 */
	private ImageIcon createImage(String string) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @return the wheatCount
	 */
	public JLabel getWheatCount() {
		return wheatCount;
	}

	/**
	 * @param wheatCount
	 *            the wheatCount to set
	 */
	public void setWheatCount(int wheatCount) {
		this.wheatCount.setText(" " + wheatCount);
	}

	/**
	 * @return the brickCount
	 */
	public JLabel getBrickCount() {
		return brickCount;
	}

	/**
	 * @param brickCount
	 *            the brickCount to set
	 */
	public void setBrickCount(int brickCount) {
		this.brickCount.setText(" " + brickCount);
	}

	/**
	 * @return the sheepCount
	 */
	public JLabel getSheepCount() {
		return sheepCount;
	}

	/**
	 * @param sheepCount
	 *            the sheepCount to set
	 */
	public void setSheepCount(int sheepCount) {
		this.sheepCount.setText(" " + sheepCount);
	}

	/**
	 * @return the oreCount
	 */
	public JLabel getOreCount() {
		return oreCount;
	}

	/**
	 * @param oreCount
	 *            the oreCount to set
	 */
	public void setOreCount(int oreCount) {
		this.oreCount.setText(" " + oreCount);
	}

	/**
	 * @return the woodCount
	 */
	public JLabel getWoodCount() {
		return woodCount;
	}

	/**
	 * @param woodCount
	 *            the woodCount to set
	 */
	public void setWoodCount(int woodCount) {
		this.woodCount.setText(" " + woodCount);
	}

	/**
	 * @return the vpCount
	 */
	public JLabel getVpCount() {
		return vpCount;
	}

	/**
	 * @param vpCount
	 *            the vpCount to set
	 */
	public void setVpCount(JLabel vpCount) {
		this.vpCount = vpCount;
	}

}
