package theView;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;

import model.Card;
import model.SettlersUtility;

public class BoardPanel extends JLayeredPane implements ActionListener {

	/**
	 * hexagons is an array list of all the hexagons on the board
	 */
	private ArrayList<HexButton> hexagons;

	public ArrayList<HexButton> getHexagons() {
		return hexagons;
	}

	/**
	 * corners is an array list of all the corners on the board
	 */
	private ArrayList<Corner> corners;
	/**
	 * clicked is a hexbutton representing the button that was clicked
	 */
	private HexButton clicked;
	/**
	 * myColor is the player's color
	 */
	private Color myColor;
	/**
	 * roads is an array list of the roads on the board
	 */
	private ArrayList<Road> roads;
	/**
	 * EMPTYSPACES are the hexagons in the view that have to be set to invisible
	 */
	private final static int[] EMPTYSPACES = new int[] { 0, 2, 20 };
	/**
	 * ports is an array list of all ports on the board
	 */
	private ArrayList<Port> ports = new ArrayList<Port>();

	/**
	 * constructs the board panel
	 */
	public BoardPanel() {
		this.setLayout(new HexLayout(9, 3));
		this.hexagons = new ArrayList<HexButton>();
		this.corners = new ArrayList<Corner>();
		this.roads = new ArrayList<Road>();
		this.addButtons();
		this.clicked.updateColor();
		this.makePorts();
	}

	/**
	 * makes all the ports on the board
	 */
	public void makePorts() {
		for (int x = 0; x < 5; x++) {
			Port port1 = new Port(Card.values()[x]);
			port1.setIcon(new ImageIcon(Card.values()[x].getPort()));
			port1.setEnabled(false);
			Port port2 = new Port(Card.values()[5]);
			port2.setIcon(new ImageIcon("src/theView/port.png"));
			this.ports.add(port1);

			this.add(port1, new Integer(4));
			if (x < 4) {
				port2.setEnabled(false);
				this.ports.add(port2);
				this.add(port2, new Integer(4));
			}
		}
	}

	public ArrayList<Port> getPorts() {
		return this.ports;
	}

	/**
	 * hides all the corners on the board
	 */
	public void hideCorners() {
		for (Corner corner : this.corners) {
			corner.setVisible(false);
		}
	}

	public void makeNeighbors() {
		this.roads.get(0).addNeighbor(this.roads.get(1));
		this.roads.get(0).addNeighbor(this.roads.get(5));
		this.roads.get(0).addNeighbor(this.roads.get(18));
		this.roads.get(0).addNeighbor(this.roads.get(19));
		this.roads.get(1).addNeighbor(this.roads.get(0));
		this.roads.get(1).addNeighbor(this.roads.get(18));
		this.roads.get(1).addNeighbor(this.roads.get(2));
		this.roads.get(1).addNeighbor(this.roads.get(8));
		this.roads.get(2).addNeighbor(this.roads.get(1));
		this.roads.get(2).addNeighbor(this.roads.get(3));
		this.roads.get(2).addNeighbor(this.roads.get(8));
		this.roads.get(3).addNeighbor(this.roads.get(2));
		this.roads.get(3).addNeighbor(this.roads.get(4));
		this.roads.get(4).addNeighbor(this.roads.get(3));
		this.roads.get(4).addNeighbor(this.roads.get(5));
		this.roads.get(4).addNeighbor(this.roads.get(10));
		this.roads.get(5).addNeighbor(this.roads.get(0));
		this.roads.get(5).addNeighbor(this.roads.get(4));
		this.roads.get(5).addNeighbor(this.roads.get(19));
		this.roads.get(5).addNeighbor(this.roads.get(10));
		this.roads.get(6).addNeighbor(this.roads.get(16));
		this.roads.get(6).addNeighbor(this.roads.get(17));
		this.roads.get(6).addNeighbor(this.roads.get(18));
		this.roads.get(6).addNeighbor(this.roads.get(27));
		this.roads.get(7).addNeighbor(this.roads.get(15));
		this.roads.get(7).addNeighbor(this.roads.get(16));
		this.roads.get(7).addNeighbor(this.roads.get(8));
		this.roads.get(8).addNeighbor(this.roads.get(1));
		this.roads.get(8).addNeighbor(this.roads.get(2));
		this.roads.get(8).addNeighbor(this.roads.get(7));
		this.roads.get(9).addNeighbor(this.roads.get(19));
		this.roads.get(9).addNeighbor(this.roads.get(29));
		this.roads.get(9).addNeighbor(this.roads.get(22));
		this.roads.get(9).addNeighbor(this.roads.get(21));
		this.roads.get(10).addNeighbor(this.roads.get(4));
		this.roads.get(10).addNeighbor(this.roads.get(5));
		this.roads.get(10).addNeighbor(this.roads.get(11));
		this.roads.get(11).addNeighbor(this.roads.get(22));
		this.roads.get(11).addNeighbor(this.roads.get(23));
		this.roads.get(11).addNeighbor(this.roads.get(10));
		this.roads.get(12).addNeighbor(this.roads.get(13));
		this.roads.get(12).addNeighbor(this.roads.get(31));
		this.roads.get(12).addNeighbor(this.roads.get(17));
		this.roads.get(12).addNeighbor(this.roads.get(32));
		this.roads.get(13).addNeighbor(this.roads.get(12));
		this.roads.get(13).addNeighbor(this.roads.get(31));
		this.roads.get(13).addNeighbor(this.roads.get(14));
		this.roads.get(14).addNeighbor(this.roads.get(13));
		this.roads.get(14).addNeighbor(this.roads.get(15));
		this.roads.get(15).addNeighbor(this.roads.get(16));
		this.roads.get(15).addNeighbor(this.roads.get(7));
		this.roads.get(15).addNeighbor(this.roads.get(14));
		this.roads.get(16).addNeighbor(this.roads.get(6));
		this.roads.get(16).addNeighbor(this.roads.get(7));
		this.roads.get(16).addNeighbor(this.roads.get(15));
		this.roads.get(16).addNeighbor(this.roads.get(17));
		this.roads.get(17).addNeighbor(this.roads.get(16));
		this.roads.get(17).addNeighbor(this.roads.get(6));
		this.roads.get(17).addNeighbor(this.roads.get(12));
		this.roads.get(17).addNeighbor(this.roads.get(32));
		this.roads.get(18).addNeighbor(this.roads.get(0));
		this.roads.get(18).addNeighbor(this.roads.get(1));
		this.roads.get(18).addNeighbor(this.roads.get(6));
		this.roads.get(18).addNeighbor(this.roads.get(27));
		this.roads.get(19).addNeighbor(this.roads.get(0));
		this.roads.get(19).addNeighbor(this.roads.get(5));
		this.roads.get(19).addNeighbor(this.roads.get(29));
		this.roads.get(19).addNeighbor(this.roads.get(9));
		this.roads.get(20).addNeighbor(this.roads.get(21));
		this.roads.get(20).addNeighbor(this.roads.get(25));
		this.roads.get(20).addNeighbor(this.roads.get(39));
		this.roads.get(20).addNeighbor(this.roads.get(40));
		this.roads.get(21).addNeighbor(this.roads.get(9));
		this.roads.get(21).addNeighbor(this.roads.get(22));
		this.roads.get(21).addNeighbor(this.roads.get(20));
		this.roads.get(21).addNeighbor(this.roads.get(39));
		this.roads.get(22).addNeighbor(this.roads.get(9));
		this.roads.get(22).addNeighbor(this.roads.get(21));
		this.roads.get(22).addNeighbor(this.roads.get(11));
		this.roads.get(22).addNeighbor(this.roads.get(23));
		this.roads.get(23).addNeighbor(this.roads.get(11));
		this.roads.get(23).addNeighbor(this.roads.get(22));
		this.roads.get(23).addNeighbor(this.roads.get(24));
		this.roads.get(24).addNeighbor(this.roads.get(23));
		this.roads.get(24).addNeighbor(this.roads.get(25));
		this.roads.get(25).addNeighbor(this.roads.get(20));
		this.roads.get(25).addNeighbor(this.roads.get(40));
		this.roads.get(25).addNeighbor(this.roads.get(24));
		this.roads.get(26).addNeighbor(this.roads.get(32));
		this.roads.get(26).addNeighbor(this.roads.get(34));
		this.roads.get(26).addNeighbor(this.roads.get(35));
		this.roads.get(26).addNeighbor(this.roads.get(43));
		this.roads.get(27).addNeighbor(this.roads.get(6));
		this.roads.get(27).addNeighbor(this.roads.get(36));
		this.roads.get(27).addNeighbor(this.roads.get(18));
		this.roads.get(27).addNeighbor(this.roads.get(35));
		this.roads.get(28).addNeighbor(this.roads.get(37));
		this.roads.get(28).addNeighbor(this.roads.get(38));
		this.roads.get(28).addNeighbor(this.roads.get(39));
		this.roads.get(28).addNeighbor(this.roads.get(45));
		this.roads.get(29).addNeighbor(this.roads.get(36));
		this.roads.get(29).addNeighbor(this.roads.get(37));
		this.roads.get(29).addNeighbor(this.roads.get(9));
		this.roads.get(29).addNeighbor(this.roads.get(19));
		this.roads.get(30).addNeighbor(this.roads.get(31));
		this.roads.get(30).addNeighbor(this.roads.get(49));
		this.roads.get(30).addNeighbor(this.roads.get(48));
		this.roads.get(31).addNeighbor(this.roads.get(12));
		this.roads.get(31).addNeighbor(this.roads.get(13));
		this.roads.get(31).addNeighbor(this.roads.get(30));
		this.roads.get(32).addNeighbor(this.roads.get(12));
		this.roads.get(32).addNeighbor(this.roads.get(17));
		this.roads.get(32).addNeighbor(this.roads.get(43));
		this.roads.get(32).addNeighbor(this.roads.get(26));
		this.roads.get(33).addNeighbor(this.roads.get(34));
		this.roads.get(33).addNeighbor(this.roads.get(38));
		this.roads.get(33).addNeighbor(this.roads.get(52));
		this.roads.get(33).addNeighbor(this.roads.get(53));
		this.roads.get(34).addNeighbor(this.roads.get(52));
		this.roads.get(34).addNeighbor(this.roads.get(33));
		this.roads.get(34).addNeighbor(this.roads.get(26));
		this.roads.get(34).addNeighbor(this.roads.get(35));
		this.roads.get(35).addNeighbor(this.roads.get(26));
		this.roads.get(35).addNeighbor(this.roads.get(27));
		this.roads.get(35).addNeighbor(this.roads.get(34));
		this.roads.get(35).addNeighbor(this.roads.get(36));
		this.roads.get(36).addNeighbor(this.roads.get(27));
		this.roads.get(36).addNeighbor(this.roads.get(29));
		this.roads.get(36).addNeighbor(this.roads.get(35));
		this.roads.get(36).addNeighbor(this.roads.get(37));
		this.roads.get(37).addNeighbor(this.roads.get(36));
		this.roads.get(37).addNeighbor(this.roads.get(38));
		this.roads.get(37).addNeighbor(this.roads.get(29));
		this.roads.get(37).addNeighbor(this.roads.get(28));
		this.roads.get(38).addNeighbor(this.roads.get(37));
		this.roads.get(38).addNeighbor(this.roads.get(33));
		this.roads.get(38).addNeighbor(this.roads.get(28));
		this.roads.get(38).addNeighbor(this.roads.get(53));
		this.roads.get(39).addNeighbor(this.roads.get(20));
		this.roads.get(39).addNeighbor(this.roads.get(21));
		this.roads.get(39).addNeighbor(this.roads.get(28));
		this.roads.get(39).addNeighbor(this.roads.get(45));
		this.roads.get(40).addNeighbor(this.roads.get(41));
		this.roads.get(40).addNeighbor(this.roads.get(25));
		this.roads.get(40).addNeighbor(this.roads.get(20));
		this.roads.get(41).addNeighbor(this.roads.get(40));
		this.roads.get(41).addNeighbor(this.roads.get(57));
		this.roads.get(41).addNeighbor(this.roads.get(58));
		this.roads.get(42).addNeighbor(this.roads.get(50));
		this.roads.get(42).addNeighbor(this.roads.get(51));
		this.roads.get(42).addNeighbor(this.roads.get(52));
		this.roads.get(42).addNeighbor(this.roads.get(62));
		this.roads.get(43).addNeighbor(this.roads.get(26));
		this.roads.get(43).addNeighbor(this.roads.get(32));
		this.roads.get(43).addNeighbor(this.roads.get(49));
		this.roads.get(43).addNeighbor(this.roads.get(50));
		this.roads.get(44).addNeighbor(this.roads.get(53));
		this.roads.get(44).addNeighbor(this.roads.get(55));
		this.roads.get(44).addNeighbor(this.roads.get(56));
		this.roads.get(44).addNeighbor(this.roads.get(64));
		this.roads.get(45).addNeighbor(this.roads.get(28));
		this.roads.get(45).addNeighbor(this.roads.get(39));
		this.roads.get(45).addNeighbor(this.roads.get(56));
		this.roads.get(45).addNeighbor(this.roads.get(57));
		this.roads.get(46).addNeighbor(this.roads.get(47));
		this.roads.get(46).addNeighbor(this.roads.get(51));
		this.roads.get(46).addNeighbor(this.roads.get(61));
		this.roads.get(47).addNeighbor(this.roads.get(46));
		this.roads.get(47).addNeighbor(this.roads.get(48));
		this.roads.get(48).addNeighbor(this.roads.get(47));
		this.roads.get(48).addNeighbor(this.roads.get(30));
		this.roads.get(48).addNeighbor(this.roads.get(49));
		this.roads.get(49).addNeighbor(this.roads.get(48));
		this.roads.get(49).addNeighbor(this.roads.get(50));
		this.roads.get(49).addNeighbor(this.roads.get(30));
		this.roads.get(49).addNeighbor(this.roads.get(43));
		this.roads.get(50).addNeighbor(this.roads.get(49));
		this.roads.get(50).addNeighbor(this.roads.get(51));
		this.roads.get(50).addNeighbor(this.roads.get(42));
		this.roads.get(50).addNeighbor(this.roads.get(43));
		this.roads.get(51).addNeighbor(this.roads.get(46));
		this.roads.get(51).addNeighbor(this.roads.get(50));
		this.roads.get(51).addNeighbor(this.roads.get(42));
		this.roads.get(51).addNeighbor(this.roads.get(61));
		this.roads.get(52).addNeighbor(this.roads.get(33));
		this.roads.get(52).addNeighbor(this.roads.get(34));
		this.roads.get(52).addNeighbor(this.roads.get(42));
		this.roads.get(52).addNeighbor(this.roads.get(62));
		this.roads.get(53).addNeighbor(this.roads.get(33));
		this.roads.get(53).addNeighbor(this.roads.get(38));
		this.roads.get(53).addNeighbor(this.roads.get(44));
		this.roads.get(53).addNeighbor(this.roads.get(64));
		this.roads.get(54).addNeighbor(this.roads.get(55));
		this.roads.get(54).addNeighbor(this.roads.get(65));
		this.roads.get(54).addNeighbor(this.roads.get(59));
		this.roads.get(55).addNeighbor(this.roads.get(54));
		this.roads.get(55).addNeighbor(this.roads.get(56));
		this.roads.get(55).addNeighbor(this.roads.get(65));
		this.roads.get(55).addNeighbor(this.roads.get(44));
		this.roads.get(56).addNeighbor(this.roads.get(44));
		this.roads.get(56).addNeighbor(this.roads.get(45));
		this.roads.get(56).addNeighbor(this.roads.get(55));
		this.roads.get(56).addNeighbor(this.roads.get(57));
		this.roads.get(57).addNeighbor(this.roads.get(41));
		this.roads.get(57).addNeighbor(this.roads.get(45));
		this.roads.get(57).addNeighbor(this.roads.get(56));
		this.roads.get(57).addNeighbor(this.roads.get(58));
		this.roads.get(58).addNeighbor(this.roads.get(41));
		this.roads.get(58).addNeighbor(this.roads.get(57));
		this.roads.get(58).addNeighbor(this.roads.get(59));
		this.roads.get(59).addNeighbor(this.roads.get(54));
		this.roads.get(59).addNeighbor(this.roads.get(58));
		this.roads.get(60).addNeighbor(this.roads.get(61));
		this.roads.get(60).addNeighbor(this.roads.get(67));
		this.roads.get(60).addNeighbor(this.roads.get(68));
		this.roads.get(61).addNeighbor(this.roads.get(46));
		this.roads.get(61).addNeighbor(this.roads.get(51));
		this.roads.get(61).addNeighbor(this.roads.get(60));
		this.roads.get(62).addNeighbor(this.roads.get(42));
		this.roads.get(62).addNeighbor(this.roads.get(52));
		this.roads.get(62).addNeighbor(this.roads.get(68));
		this.roads.get(62).addNeighbor(this.roads.get(69));
		this.roads.get(63).addNeighbor(this.roads.get(70));
		this.roads.get(63).addNeighbor(this.roads.get(71));
		this.roads.get(63).addNeighbor(this.roads.get(65));
		this.roads.get(64).addNeighbor(this.roads.get(44));
		this.roads.get(64).addNeighbor(this.roads.get(53));
		this.roads.get(64).addNeighbor(this.roads.get(69));
		this.roads.get(64).addNeighbor(this.roads.get(70));
		this.roads.get(65).addNeighbor(this.roads.get(54));
		this.roads.get(65).addNeighbor(this.roads.get(55));
		this.roads.get(65).addNeighbor(this.roads.get(63));
		this.roads.get(66).addNeighbor(this.roads.get(67));
		this.roads.get(66).addNeighbor(this.roads.get(71));
		this.roads.get(67).addNeighbor(this.roads.get(66));
		this.roads.get(67).addNeighbor(this.roads.get(68));
		this.roads.get(67).addNeighbor(this.roads.get(60));
		this.roads.get(68).addNeighbor(this.roads.get(60));
		this.roads.get(68).addNeighbor(this.roads.get(67));
		this.roads.get(68).addNeighbor(this.roads.get(69));
		this.roads.get(68).addNeighbor(this.roads.get(62));
		this.roads.get(69).addNeighbor(this.roads.get(62));
		this.roads.get(69).addNeighbor(this.roads.get(64));
		this.roads.get(69).addNeighbor(this.roads.get(68));
		this.roads.get(69).addNeighbor(this.roads.get(70));
		this.roads.get(70).addNeighbor(this.roads.get(64));
		this.roads.get(70).addNeighbor(this.roads.get(69));
		this.roads.get(70).addNeighbor(this.roads.get(63));
		this.roads.get(70).addNeighbor(this.roads.get(71));
		this.roads.get(71).addNeighbor(this.roads.get(70));
		this.roads.get(71).addNeighbor(this.roads.get(66));
		this.roads.get(71).addNeighbor(this.roads.get(63));
	}

	/**
	 * builds a road
	 * 
	 * @param owned
	 *            - the roads the current player owns already
	 */
	public void buildRoad(ArrayList<JButton> owned) {
		int bound;
		for (JButton taken : owned) {
			bound = 40 * this.hexagons.get(1).getHeight() / 100;
			if (taken instanceof Road) {
				for (Road neighbor : ((Road) taken).getNeighbors()) {
					if (neighbor.getBackground().equals(Color.BLACK)) {
						neighbor.setBackground(Color.GREEN);
						neighbor.setEnabled(true);
						neighbor.setVisible(true);
					}
				}
			}
		}
	}

	/**
	 * builds a road
	 * 
	 * @param corner
	 */
	public void buildRoad(JButton corner) {
		int bound = 40 * this.hexagons.get(1).getHeight() / 100;
		for (Road road : this.roads) {
			if (SettlersUtility.inBounds(corner.getX(), corner.getY(), road, bound)
					&& road.getBackground().equals(Color.BLACK)) {
				road.setBackground(Color.GREEN);
				road.setEnabled(true);
				road.setVisible(true);
			}
		}
	}

	/**
	 * hides roads on the board
	 */
	public void hideRoads() {
		for (Road road : this.roads) {
			if (road.getBackground().equals(Color.BLACK)
					|| road.getBackground().equals(Color.GREEN)) {
				road.setEnabled(false);
				road.setBackground(Color.BLACK);
				road.setVisible(false);
			}
		}
	}

	/**
	 * 
	 * @param roadsBuilt
	 * @return boolean indicating if you can build a settlement
	 */
	public boolean canBuildSettlement(ArrayList<JButton> roadsBuilt) {
		int bound;
		for (JButton taken : roadsBuilt) {
			if (!(taken instanceof Road))
				continue;
			bound = 38 * this.hexagons.get(0).getWidth() / 100;
			for (Corner corner : this.getCorners()) {
				if (SettlersUtility.inBounds(taken.getX(), taken.getY(),
						corner, bound) && corner.getColor().equals(Color.GREEN)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * shows all available corners
	 * 
	 * @param taken
	 */
	public void showAvailableCorners(Corner taken) {
		ArrayList<Corner> removed = new ArrayList<Corner>();
		int bound = 3 * this.hexagons.get(0).getWidth() / 5;
		this.corners.remove(taken);
		for (Corner corner : this.getCorners()) {
			if (SettlersUtility.inBounds(taken.getX(), taken.getY(), corner,
					bound)) {
				corner.setVisible(false);
				removed.add(corner);
			}
		}
		for (Corner remove : removed) {
			this.getCorners().remove(remove);
		}
	}

	/**
	 * builds a settlement
	 * 
	 * @param roadsBuilt
	 */
	public void buildSettlement(ArrayList<JButton> roadsBuilt) {
		int bound;
		for (JButton taken : roadsBuilt) {
			if (!(taken instanceof Road))
				continue;
			bound = 38 * this.hexagons.get(0).getWidth() / 100;
			for (Corner corner : this.getCorners()) {
				if (SettlersUtility.inBounds(taken.getX(), taken.getY(),
						corner, bound) && corner.getColor().equals(Color.GREEN)) {
					corner.setEnabled(true);
					corner.setVisible(true);
				}
			}
		}
	}

	/**
	 * shows all corners
	 */
	public void showCorners() {
		for (Corner corner : this.getCorners()) {
			corner.setVisible(true);
		}
	}

	/**
	 * moves the robber
	 */
	public void moveRobber() {
		for (HexButton tile : this.hexagons) {
			if (!tile.equals(this.clicked))
				tile.setEnabled(true);
		}
	}

	/**
	 * disables all tiles
	 */
	public void disableTiles() {
		for (HexButton tile : this.hexagons) {
			tile.setEnabled(false);
		}
	}

	/**
	 * makes colors for the board and randomizes them in an array list
	 * 
	 * @return aforementioned array list
	 */
	public ArrayList<String> getRandomColors() {
		ArrayList<String> tileColors = new ArrayList<String>(19);
		ArrayList<String> mixedTileColors = new ArrayList<String>();
		Random generator = new Random();

		int removed = 0, totalNums = 0;
		for (int x = 0; x < 19; x++) {
			if (x < 3)
				tileColors.add(Card.ORE.getTile());
			else if (x < 7)
				tileColors.add(Card.BRICK.getTile());
			else if (x < 11)
				tileColors.add(Card.SHEEP.getTile());
			else if (x < 15)
				tileColors.add(Card.WHEAT.getTile());
			else
				tileColors.add(Card.WOOD.getTile());
		}
		totalNums = tileColors.size();
		for (int x = 0; x < totalNums; x++) {
			removed = generator.nextInt(tileColors.size());
			mixedTileColors.add(tileColors.get(removed));
			tileColors.remove(removed);
		}
		return mixedTileColors;
	}

	/**
	 * adds buttons
	 */
	public void addButtons() {
		ArrayList<Integer> numbers = getRandomNunmbers();
		ArrayList<String> tileColors = getRandomColors();
		for (int x = 0; x < 22; x++) {
			HexButton hb = new HexButton();
			JLabel number = new JLabel();
			for (int emptySpace : this.EMPTYSPACES) {
				if (x == emptySpace) {
					hb.setVisible(false);
				}
			}
			if (hb.isVisible()) {
				if (numbers.get(0) != -1) {
					hb.setResource(tileColors.get(0));
					tileColors.remove(0);
				} else
					hb.setBackground(Color.LIGHT_GRAY);
				if (numbers.size() > 0) {
					if (numbers.get(0) != -1) {
						hb.setMyNumber(numbers.get(0));
						number.setText(numbers.get(0) + "");
						JLabel newLabel = new JLabel("    " + numbers.get(0));
						newLabel.setVisible(false);
						number.setFont(new Font("Serif", Font.BOLD, 20));
						if (numbers.get(0) == 6 || numbers.get(0) == 8)
							number.setForeground(Color.RED);
						hb.add(newLabel, BorderLayout.CENTER);
					} else
						this.clicked = hb;
					numbers.remove(0);
				}
			}
			this.add(number, new Integer(5));
			this.add(hb, new Integer(1));
			this.addCorners(x, hb);
			if (hb.isVisible())
				this.hexagons.add(hb);
		}
	}

	/**
	 * adds corners
	 * 
	 * @param x
	 * @param hb
	 */
	public void addCorners(int x, HexButton hb) {
		for (int k = 0; k < 6; k++) {
			Corner corner = new Corner(Color.GREEN);
			Road road = new Road(k);
			road.setColor(Color.GREEN);
			if (!hb.isVisible() || x % 5 == 3 || x % 5 == 4 || x % 10 == 6
					|| x == 10 || x == 12) {
				corner.setVisible(false);
				road.setVisible(false);
			}
			if ((x == 3 && k == 3) || (x == 4 && k == 4) || (x == 18 && k == 1)
					|| (x == 19 && k == 0)
					|| ((x == 6 || x == 16) && (k == 2 || k == 5))
					|| ((x == 10 || x == 12) && (k == 5 || k == 2))) {
				corner.setVisible(true);
			}
			if (hb.isVisible()
					&& ((x < 5 && k == 3) || ((x == 12 || x == 19) && k == 5)
							|| ((x == 10 || x == 18) && k == 1)
							|| ((x % 5 == 3 || x % 5 == 4) && k == 0) || (x % 2 == 0
							&& k == 4 || ((x % 3 == 0 || ((x - 7) % 3) == 0)
							&& x != 18 && k == 2)))) {
				road.setVisible(true);
			}
			if (x == 4 && k == 2)
				road.setVisible(false);
			if (corner.isVisible()) {
				corner.addActionListener(this);
				this.corners.add(corner);
			}
			if (road.isVisible()) {
				road.addActionListener(this);
				this.roads.add(road);
			}
			road.setBackground(Color.BLACK);
			road.setFont(new Font("Serif", Font.BOLD, 20));
			this.add(road, new Integer(2));
			this.add(corner, new Integer(3));
		}
	}

	/**
	 * gets the random numbers to put on the board
	 * 
	 * @return an array list contatining the random numbers
	 */
	public ArrayList<Integer> getRandomNunmbers() {
		ArrayList<Integer> numbers = new ArrayList<Integer>();
		ArrayList<Integer> mixedNums = new ArrayList<Integer>();
		Random generator = new Random();
		int removed = 0, totalNums = 0;
		numbers.add(2);
		numbers.add(12);
		for (int x = 3; x < 12; x++) {
			if (x != 7) {
				numbers.add(x);
				numbers.add(x);
			}
		}
		totalNums = numbers.size();
		for (int x = 0; x < totalNums; x++) {
			removed = generator.nextInt(numbers.size());
			mixedNums.add(numbers.get(removed));
			numbers.remove(removed);
		}
		mixedNums.add(generator.nextInt(mixedNums.size()), -1);
		return mixedNums;
	}

	public void setColor(Color color) {
		this.myColor = color;
	}

	public Color getColor() {
		return this.myColor;
	}

	public HexButton getClicked() {
		return this.clicked;
	}

	public void setClciked(HexButton hexbutton) {
		this.clicked = hexbutton;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

	public ArrayList<Corner> getCorners() {
		return corners;
	}

	public ArrayList<Road> getRoads() {
		return roads;
	}
}
