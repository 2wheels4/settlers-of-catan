/**
 * CSCI 205 - Software Design and Engineering
 * Name(s): Cole Conte
 * 
 * Work:	Settlers
 * Created:	Dec 4, 2014
 */
package theView;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import model.Board;

/**
 * @author coleconte
 * 
 *
 */
public class StartUpPane extends JFrame {
	private JLabel player4 = new JLabel("Player 4:");
	private JComboBox color1;
	private JComboBox color2;
	private JComboBox color3;
	private JComboBox color4;
	private JButton startGame = new JButton("Start Game");
	private JButton addPlayer = new JButton("Add Player");
	private JLabel cantStart = new JLabel(
			"Each player must be a different color!");

	/**
	 * @return the cantStart
	 */
	public JLabel getCantStart() {
		return cantStart;
	}

	/**
	 * @return the player4
	 */
	public JLabel getPlayer4() {
		return player4;
	}

	/**
	 * @return the color1
	 */
	public JComboBox getColor1() {
		return color1;
	}

	/**
	 * @return the color2
	 */
	public JComboBox getColor2() {
		return color2;
	}

	/**
	 * @return the color3
	 */
	public JComboBox getColor3() {
		return color3;
	}

	/**
	 * @return the color4
	 */
	public JComboBox getColor4() {
		return color4;
	}

	/**
	 * @return the startGame
	 */
	public JButton getStartGame() {
		return startGame;
	}

	/**
	 * @return the addPlayer
	 */
	public JButton getAddPlayer() {
		return addPlayer;
	}

	public StartUpPane(Board board) {
		String[] colors = { "magenta", "orange", "blue", "pink" };
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Settlers of Catan: Start Screen");
		setLocationRelativeTo(null);
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(6, 2));
		panel.add(new JLabel("Player 1:"));
		color1 = new JComboBox(colors);
		color1.setSelectedIndex(0);
		panel.add(color1);
		panel.add(new JLabel("Player 2:"));
		color2 = new JComboBox(colors);
		color2.setSelectedIndex(1);
		panel.add(color2);
		panel.add(new JLabel("Player 3:"));
		color3 = new JComboBox(colors);
		color3.setSelectedIndex(2);
		panel.add(color3);
		player4.setVisible(false);
		panel.add(player4);
		color4 = new JComboBox(colors);
		color4.setSelectedIndex(3);
		color4.setVisible(false);
		panel.add(color4);
		panel.add(startGame);
		panel.add(addPlayer);
		this.cantStart.setVisible(false);
		panel.add(cantStart);
		this.add(panel, BorderLayout.CENTER);
		this.pack();
		this.setVisible(true);
	}
}
