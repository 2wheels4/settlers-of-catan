/**
 * CSCI 205 - Software Design and Engineering
 * Name(s): Andrew Wheeler
 *  
 *  Work:	Settlers
 *  Created Nov 22, 2014, 12:39:10 PM
 */
package theView;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

import javax.swing.JButton;

/**
 * @author asw011
 *
 */
public class Corner extends JButton {

	/**
	 * myColor is the corner's color
	 */
	private Color myColor;
	/**
	 * isSettlement is a boolean describing if the corner is a settlement
	 */
	private boolean isSettlement;
	/**
	 * clicks is the number of clicks on the corner
	 */
	private int clicks = 0;

	/**
	 * constructs a corner
	 * 
	 * @param myColor
	 */
	public Corner(Color myColor) {
		this.myColor = myColor;
		this.isSettlement = true;
		this.setBorderPainted(false);
		this.setOpaque(true);
	}

	public Color getColor() {
		return this.myColor;
	}

	public void setColor(Color color) {
		this.myColor = color;
	}

	/**
	 * creates a pentagon
	 * 
	 * @param width
	 * @param height
	 * @return the pentago
	 */
	private Polygon pentagon(int width, int height) {
		Polygon pentagon = new Polygon();
		pentagon.addPoint(width * 5 / 6, height);
		pentagon.addPoint(width / 6, height);
		pentagon.addPoint(width / 6, height * 1 / 3);
		pentagon.addPoint(width / 2, 0);
		pentagon.addPoint(width * 5 / 6, height * 1 / 3);
		return pentagon;
	}

	/**
	 * makes a city
	 * 
	 * @param width
	 * @param height
	 * @return the city
	 */
	private Polygon city(int width, int height) {
		Polygon city = new Polygon();
		city.addPoint(width, height);
		city.addPoint(0, height);
		city.addPoint(0, height / 2);
		city.addPoint(width / 2, height / 2);
		city.addPoint(width / 2, height / 6);
		city.addPoint(width * 3 / 4, 0);
		city.addPoint(width, height / 6);
		return city;
	}

	public void setCity() {
		this.isSettlement = false;
	}

	@Override
	protected void paintComponent(Graphics graphics) {
		Polygon inside;
		if (this.isSettlement())
			inside = this.pentagon(getWidth(), getHeight());
		else
			inside = this.city(getWidth(), getHeight());
		graphics.setColor(this.getColor());
		graphics.drawPolygon(inside);
		graphics.fillPolygon(inside);

	}

	public boolean isSettlement() {
		return isSettlement;
	}

}
