/**
 * CSCI 205 - Software Design and Engineering
 * Name(s): Cole Conte
 * 
 * Work:	Settlers
 * Created:	Dec 8, 2014
 */
package model;

import java.util.ArrayList;
import java.util.Collections;

/**
 * @author coleconte
 *
 */
public class Deck {
	/**
	 * cards is a list of all the cards in the deck
	 */
	private ArrayList<Card> cards = new ArrayList<Card>();

	/**
	 * constructor for the deck
	 */
	public Deck() {
		cards.add(Card.YEAROFPLENTY);
		cards.add(Card.YEAROFPLENTY);
		cards.add(Card.MONOPOLY);
		cards.add(Card.MONOPOLY);
		cards.add(Card.ROADBUILDING);
		cards.add(Card.ROADBUILDING);
		cards.add(Card.SOLDIER);
		for (int i = 0; i < 14; i++) {
			cards.add(Card.SOLDIER);
		}
		cards.add(Card.UNIVERSITYOFCATAN);
		cards.add(Card.CHAPEL);
		cards.add(Card.GOVERNORSHOUSE);
		cards.add(Card.LIBRARY);
		cards.add(Card.MARKET);
		Collections.shuffle(cards);
	}

	/**
	 * @return the cards
	 */
	public ArrayList<Card> getCards() {
		return cards;
	}

	/**
	 * @return the cards
	 */
	public Card getFrontCardAndRemoveFromDeck() {
		Card card = cards.get(0);
		cards.remove(0);
		return card;
	}

}
