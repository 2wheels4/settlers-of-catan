/**
 * CSCI 205 - Software Design and Engineering
 * Name(s): Seline Tan-Torres
 *
 * Work: Final Project
 * Created: Nov 18, 2014, 12:31:01 PM
 */
package model;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JButton;

/**
 * @author satt001
 *
 *         Represents a player in the game
 */
public class Player {

	/** the Color associated with this instance of Player */
	private Color myColor;
	/** the board this Player is associated with */
	private Board theBoard;
	/**
	 * an ArrayList of integers representing the number of each card the player
	 * has in order of sheep, wheat, brick, wood, ore
	 */
	private ArrayList<Integer> myHand;
	/**
	 * an array list containing all roads, settlements and cities that the
	 * player owns
	 */
	private ArrayList<JButton> myButtons;
	/** an array list of all development cards the player has */
	private ArrayList<Card> DevCards = new ArrayList<Card>();
	/** the number of settlements the player has */
	private int settlements = 2;
	/** the number of cities the player has */
	private int cities = 0;

	private int longestRoad = 2;

	private int VPcount = 2;
	/** size of player's army */
	private int knights = 0;

	/**
	 * constructor for the Player class
	 * 
	 * @param theBoard
	 *            - the board associated with this player
	 * @param myColor
	 *            - the color associated with this player
	 */
	public Player(Board theBoard, Color myColor) {
		this.theBoard = theBoard;
		this.myColor = myColor;
		this.myButtons = new ArrayList<JButton>();
		myHand = new ArrayList<Integer>();
		for (Card card : Card.values()) {
			myHand.add(0);
		}
	}

	/**
	 * removes the resources associated with building a city, and updates the
	 * victory point count
	 */
	public void buildCity() {
		cities += 1;
		this.myHand.set(Card.ORE.ordinal(),
				this.myHand.get(Card.ORE.ordinal()) - 3);
		this.myHand.set(Card.WHEAT.ordinal(),
				this.myHand.get(Card.WHEAT.ordinal()) - 2);
		this.settlements--;
		this.VPcount++;
	}

	/**
	 * removes the resources associated with building a settlement, and updates
	 * the victory point count
	 */
	public void buildSettlement() {
		settlements += 1;
		this.myHand.set(Card.BRICK.ordinal(),
				this.myHand.get(Card.BRICK.ordinal()) - 1);
		this.myHand.set(Card.WHEAT.ordinal(),
				this.myHand.get(Card.WHEAT.ordinal()) - 1);
		this.myHand.set(Card.WOOD.ordinal(),
				this.myHand.get(Card.WOOD.ordinal()) - 1);
		this.myHand.set(Card.SHEEP.ordinal(),
				this.myHand.get(Card.SHEEP.ordinal()) - 1);
		this.VPcount++;
	}

	/**
	 * removes the resources associated with building a road
	 */
	public void buildRoad() {
		this.myHand.set(Card.WOOD.ordinal(),
				this.myHand.get(Card.WOOD.ordinal()) - 1);
		this.myHand.set(Card.BRICK.ordinal(),
				this.myHand.get(Card.BRICK.ordinal()) - 1);
		this.longestRoad += 1;
	}

	/**
	 * This method counts the resource cards in the player's hand
	 * 
	 * @return the amount of resource cards in the hand
	 */
	public int countResourceCards() {
		int count = 0;
		for (int x : this.getMyHand().subList(0, 5)) {
			count += x;
		}
		return count;
	}

	/**
	 * adds a resource to the player's hand
	 * 
	 * @param resource
	 *            - the resource to add
	 */
	public void addResource(Card resource) {
		this.myHand.set(resource.ordinal(),
				this.myHand.get(resource.ordinal()) + 1);
	}

	/**
	 * adds a development card to the player's hand
	 * 
	 * @param dev
	 *            - the development card to add
	 */
	public void buyDevelopmentCard() {
		Card card = this.theBoard.getDeck().getFrontCardAndRemoveFromDeck();
		this.DevCards.add(card);
		this.myHand.set(Card.SHEEP.ordinal(),
				this.myHand.get(Card.SHEEP.ordinal()) - 1);
		this.myHand.set(Card.WHEAT.ordinal(),
				this.myHand.get(Card.WHEAT.ordinal()) - 1);
		this.myHand.set(Card.ORE.ordinal(),
				this.myHand.get(Card.ORE.ordinal()) - 1);
	}

	/**
	 * gets rid of a resource from a player's hand
	 * 
	 * @param resource
	 *            - the card to remove
	 */
	public void removeResource(Card resource) {
		this.myHand.remove(resource);
	}

	/**
	 * Selects and removes a random resource from the player's hand called when
	 * the robber is moved and their settlement is selected
	 * 
	 * @return the card being removed
	 */
	public Card removeRandomCard() {
		if (this.countResourceCards() > 0) {
			Random generator = new Random();
			int resource0 = this.myHand.get(0);
			int resource1 = this.myHand.get(1);
			int resource2 = this.myHand.get(2);
			int resource3 = this.myHand.get(3);
			int resource4 = this.myHand.get(4);
			int spot = generator.nextInt(resource0 + resource1 + resource2
					+ resource3 + resource4);
			if (spot < resource0) {
				this.getMyHand().set(0, resource0 - 1);
				return Card.values()[0];
			} else if (spot < resource0 + resource1) {
				this.getMyHand().set(1, resource1 - 1);
				return Card.values()[1];
			} else if (spot < resource0 + resource1 + resource2) {
				this.getMyHand().set(2, resource2 - 1);
				return Card.values()[2];
			} else if (spot < resource0 + resource1 + resource2 + resource3) {
				this.getMyHand().set(3, resource3 - 1);
				return Card.values()[3];
			} else {
				this.getMyHand().set(4, resource4 - 1);
				return Card.values()[4];
			}
		} else {
			return null;
		}
	}

	/**
	 * gets rid of a development card from a player's hand
	 * 
	 * @param dev
	 *            - the card to remove
	 */
	public void removeDevcard(Card dev) {
		this.DevCards.remove(dev);
	}

	/**
	 * checks if the player has won
	 * 
	 * @return boolean indicating if the player has won
	 */
	public boolean hasWon() {
		if (VPcount == 10) {
			return true;
		} else
			return false;
	}

	/**
	 * increments number of knights by one
	 */

	public void incKnights() {
		this.knights++;
	}

	/**
	 * @return the myColor
	 */
	public Color getMyColor() {
		return myColor;
	}

	/**
	 * @return the theBoard
	 */
	public Board getTheBoard() {
		return theBoard;
	}

	/**
	 * @return the myHand
	 */
	public ArrayList<Integer> getMyHand() {
		return myHand;
	}

	/**
	 * @return the devCards
	 */
	public ArrayList<Card> getDevCards() {
		return DevCards;
	}

	/**
	 * @return the settlements
	 */
	public int getSettlements() {
		return settlements;
	}

	/**
	 * @return the cities
	 */
	public int getCities() {
		return cities;
	}

	/**
	 * @return the roads
	 */
	public int getLongestRoad() {
		return longestRoad;
	}

	/**
	 * @return the vPcount
	 */
	public int getVPcount() {
		return VPcount;
	}

	/**
	 * @return the knights
	 */
	public int getKnights() {
		return knights;
	}

	public ArrayList<JButton> getMyButtons() {
		return myButtons;
	}

	/**
	 * adds a new road, settlement or city button to the player's myButtons
	 * 
	 * @param button
	 *            - the button to add
	 */
	public void addOwnedButtons(JButton button) {
		this.myButtons.add(button);
	}

	/**
	 * adds a point to the victory point count
	 */
	public void addVP() {
		this.VPcount++;
	}

	/**
	 * discards resources until the player only has half left
	 */
	public void discardCards() {

		int resource0 = this.myHand.get(0);
		int resource1 = this.myHand.get(1);
		int resource2 = this.myHand.get(2);
		int resource3 = this.myHand.get(3);
		int resource4 = this.myHand.get(4);
		int currentCards = resource0 + resource1 + resource2 + resource3
				+ resource4;
		if (currentCards > 7) {
			while (resource0 + resource1 + resource2 + resource3 + resource4 > (currentCards + 1) / 2) {
				Random rand = new Random();
				int discardIndex = -1;
				int discardNum = rand.nextInt(resource0 + resource1 + resource2
						+ resource3 + resource4);
				if (discardNum < resource0) {
					discardIndex = 0;
					resource0--;
				} else if (discardNum < resource0 + resource1) {
					discardIndex = 1;
					resource1--;
				} else if (discardNum < resource0 + resource1 + resource2) {
					discardIndex = 2;
					resource2--;
				} else if (discardNum < resource0 + resource1 + resource2
						+ resource3) {
					discardIndex = 3;
					resource3--;
				} else if (discardNum < resource0 + resource1 + resource2
						+ resource3 + resource4) {
					discardIndex = 4;
					resource4--;
				}
				int num = myHand.get(discardIndex);
				num--;
				myHand.set(discardIndex, num);
			}
		}
	}
}
