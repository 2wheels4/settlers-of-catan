/**
 * CSCI 205 - Software Design and Engineering
 * Name(s): Seline Tan-Torres
 *
 * Work: Final Project
 * Created: Nov 18, 2014, 12:48:24 PM
 */
package model;

import java.awt.Color;

/**
 * @author satt001
 * 
 *         Enum class that represents each of resource and development cards
 *
 */
public enum Card {
	SHEEP("src/theView/sheep.png", Color.GREEN, "src/theView/sheepPort.png",
			"src/theView/sheeptile.png"), WHEAT("src/theView/wheat.png",
			Color.YELLOW, "src/theView/wheatPort.png",
			"src/theView/wheattile.png"), BRICK("src/theView/brick.png",
			Color.RED, "src/theView/brickPort.png", "src/theView/bricktile.png"), WOOD(
			"src/theView/wood.png", new Color(156, 93, 82),
			"src/theView/woodPort.png", "src/theView/woodtile.png"), ORE(
			"src/theView/ore.png", Color.darkGray, "src/theView/orePort.png",
			"src/theView/oretile.png"), DESERT("src/theView/deserttile.png"), KNIGHT(
			"image of knight"), CHAPEL("src/theView/chapel.png"), GOVERNORSHOUSE(
			"src/theView/governorsHouse.png"), LIBRARY(
			"src/theView/library.png"), MARKET("src/theView/market.png"), MONOPOLY(
			"src/theView/monopoly.png"), ROADBUILDING(
			"src/theView/roadBuilding.png"), SOLDIER("src/theView/soldier.png"), UNIVERSITYOFCATAN(
			"src/theView/universityOfCatan.png"), YEAROFPLENTY(
			"src/theView/yearOfPlenty.png");

	/**
	 * img is the image of the card
	 */

	private final String img;

	/**
	 * color is the color associated with the card
	 */

	private final Color color;

	/**
	 * port is the image of the associated port
	 */

	private final String port;

	private final String tile;

	/**
	 * Constructs a card enumeration
	 * 
	 * @param img
	 * @param color
	 * @param port
	 */
	Card(String img, Color color, String port, String tile) {
		this.img = img;
		this.color = color;
		this.port = port;
		this.tile = tile;
	}

	Card(String img) {
		this.img = img;
		this.tile = img;
		this.port = "";
		this.color = Color.WHITE;
	}

	public String getImage() {
		return this.img;
	}

	public String getTile() {
		return this.tile;
	}

	/**
	 * Returns the card's port
	 * 
	 * @return the port
	 */
	public String getPort() {
		return this.port;
	}

}
