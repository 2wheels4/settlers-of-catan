/**
 * CSCI 205 - Software Design and Engineering
 * Name(s): Seline Tan-Torres
 *
 * Work: Final Project
 * Created: Nov 18, 2014, 8:27:01 PM
 */
package model;

import java.util.ArrayList;

/**
 * @author satt001
 * 
 *         Represents the board on which the game is played. Adds players, sets
 *         next player, gets current player
 *
 */
public class Board {
	/**
	 * players- an array list containing all game players currentPlayer- the
	 * instance of Player that is currently in control of the board
	 */
	private ArrayList<Player> players = new ArrayList<Player>();
	private Player currentPlayer;
	private Deck deck = new Deck();

	/**
	 * @return the deck
	 */
	public Deck getDeck() {
		return deck;
	}

	/**
	 * Constructs the board
	 */
	public Board() {
		this.players = new ArrayList<Player>();
	}

	/**
	 * 
	 * @return
	 */
	public ArrayList<Player> getPlayers() {
		return players;
	}

	/**
	 * adds a new Player to the game
	 * 
	 * @param p
	 *            - the new player
	 */

	public void addPlayer(Player p) {
		players.add(p);
	}

	/**
	 * changes the current player to be the next player in the list of players
	 */

	public void nextTurn() {
		this.currentPlayer = players.get((players.indexOf(currentPlayer) + 1)
				% players.size());
	}

	/**
	 * sets current player to a specific index of our list of players
	 * 
	 * @param index
	 *            - the index to use
	 */
	public void setCurrentPlayer(int index) {
		this.currentPlayer = players.get(index);
	}

	/**
	 * @return the current player
	 */
	public Player getCurrentPlayer() {
		return this.currentPlayer;
	}

}
