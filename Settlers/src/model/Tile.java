package model;

import theView.HexButton;

public class Tile {
	/**
	 * number- the die roll corresponding to the tile myResource- the resource
	 * corresponding to the tile position- the position of the tile on the board
	 * myButton- the button corresponding to the tile
	 */
	private int number;
	private Card myResource;
	private int[] position;
	private HexButton myButton;

	/**
	 * constructor for tile
	 * 
	 * @param hexButton
	 *            - the button the tile corresponds to
	 */
	public Tile(HexButton hexButton) {
		position = new int[] { hexButton.getX() + hexButton.getWidth() / 2,
				hexButton.getY() + hexButton.getHeight() / 2 };
		number = hexButton.getMyNumber();

		if (hexButton.getResource().equals(Card.ORE.getTile())) {
			myResource = Card.ORE;
		} else if (hexButton.getResource().equals(Card.SHEEP.getTile())) {
			myResource = Card.SHEEP;
		} else if (hexButton.getResource().equals(Card.WHEAT.getTile())) {
			myResource = Card.WHEAT;
		} else if (hexButton.getResource().equals(Card.BRICK.getTile())) {
			myResource = Card.BRICK;
		} else if (hexButton.getResource().equals(Card.WOOD.getTile())) {
			myResource = Card.WOOD;
		} else {
			myResource = Card.DESERT;
		}

		this.myButton = hexButton;
	}

	public HexButton getMyButton() {
		return myButton;
	}

	public int getNumber() {
		return number;
	}

	public Card getMyResource() {
		return myResource;
	}

	public int[] getPosition() {
		return position;
	}

}
