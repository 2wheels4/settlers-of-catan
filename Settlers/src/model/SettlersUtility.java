package model;

import java.util.Random;

import javax.swing.JButton;

/**
 * CSCI 205 - Software Design and Engineering
 * Name(s): Andrew Wheeler
 *  
 *  Work:	Settlers
 *  Created Dec 1, 2014, 3:21:34 PM
 */

/**
 * @author asw011
 *
 */
public class SettlersUtility {

	/**
	 * checks if JButton testing is within a square of side size myBound of
	 * center coordinates (centerX,centerY)
	 * 
	 * @param centerX
	 *            - x coordinate of the center
	 * @param centerY
	 *            - y coordinate of the center
	 * @param testing
	 *            - JButton to check if it's within bounds
	 * @param myBound
	 *            - side size of the square
	 * @return
	 */
	public static boolean inBounds(int centerX, int centerY, JButton testing,
			int myBound) {
		int myX = testing.getX() + testing.getWidth() / 2;
		int myY = testing.getY() + testing.getHeight() / 2;
		int yBoundL = centerY - myBound;
		int yBoundH = centerY + myBound;
		int xBoundL = centerX - myBound;
		int xBoundH = centerX + myBound;
		return (myX < xBoundH && myX > xBoundL && myY < yBoundH && myY > yBoundL);

	}

	/**
	 * rolls the die
	 * 
	 * @return integer representing the roll
	 */
	public static int rollDie() {
		Random rand = new Random();
		int firstRoll = rand.nextInt(6) + 1;
		int secondRoll = rand.nextInt(6) + 1;
		return firstRoll + secondRoll;
	}
}
