import java.awt.EventQueue;

import model.Board;
import theView.StartUpPane;
import controller.StartUpPaneController;

/**
 * CSCI 205 - Software Design and Engineering
 * Name(s): Seline Tan-Torres, Andrew Wheeler, Cole Conte
 *
 * Work: Settlers
 * Created: Nov 22, 2014, 3:16:59 PM
 */

/**
 * @author satt001
 *
 */

public class SettlersMainGame {
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					Board board = new Board();
					StartUpPane start = new StartUpPane(board);
					StartUpPaneController sController = new StartUpPaneController(
							board, start);
					start.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
