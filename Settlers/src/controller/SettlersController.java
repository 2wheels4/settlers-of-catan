/**
 * CSCI 205 - Software Design and Engineering
 * Name(s): Andrew Wheeler
 *  
 *  Work:	Settlers
 *  Created Nov 21, 2014, 9:50:41 AM
 */
package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.ArrayList;

import javax.swing.JButton;

import model.Board;
import model.Card;
import model.Player;
import model.SettlersUtility;
import model.Tile;
import theView.Corner;
import theView.DevelopmentCardPopUp;
import theView.HexButton;
import theView.MainView;
import theView.Port;
import theView.Road;

/**
 * @author asw011
 *
 */
public class SettlersController implements ActionListener, ComponentListener {

	/** theView is the main view of the game */
	private MainView theView;
	/** theBoard is the representation of the board in the game */
	private Board theBoard;
	/** this ArrayList holds the tiles created from the board's hexagons */
	private ArrayList<Tile> tiles;
	/** this holds all of the settlements and cities the players own */
	private ArrayList<Corner> cornersTaken = new ArrayList<Corner>();;
	/** these numbers represent the height and width of the hexagons */
	private int hexWidth, hexHeight;
	/**
	 * setUp indicates if the board is in setup mode and is used by the action
	 * listener tradingOne indicates if the game is in the first part of trading
	 * where the player selects the cards they are giving away tradingTwo
	 * indicates if the game is in the second part of trading where the player
	 * selects the card to receive portTrade indicates if the player is trading
	 * with a port robbing indicates if the player is moving the robber disable
	 * tells the controller to disable a lot of buttons when the player is
	 * moving
	 * */
	private boolean setUp = true, tradingOne = false, tradingTwo = false,
			portTrade = false, robbing = false, disable = false;
	/**
	 * startUpnum indicates how many settlements have been made at the start
	 * portTake tells the controller how many cards it takes to trade
	 */
	private int startUpNum = 0, portTake = 0;

	public SettlersController(MainView theView, Board theBoard) {
		this.theBoard = theBoard;
		this.theBoard.setCurrentPlayer(0);
		this.theView = theView;
		this.theView.addComponentListener(this);
		this.hexHeight = this.theView.getBoardPanel().getHexagons().get(0)
				.getHeight();
		this.hexWidth = this.theView.getBoardPanel().getHexagons().get(0)
				.getWidth();
		this.tiles = makeTiles();
		updatePlayerPanel();
		addActionListeners();
		disable();
		this.theView.getBoardPanel().makeNeighbors();
	}

	/**
	 * addActionListeners adds action listeners to all of the components in the
	 * main view
	 */
	public void addActionListeners() {
		this.theView.getOptionsPanel().getTradeBtn().addActionListener(this);
		this.theView.getOptionsPanel().getEndTurnBtn().addActionListener(this);
		for (JButton button : this.theView.getHandPanel().getHandPanelButtons()) {
			button.addActionListener(this);
		}
		this.theView.getBuildPanel().getDevBtn().addActionListener(this);
		this.theView.getBuildPanel().getSeeDevelopmentCards()
				.addActionListener(this);
		this.theView.getBuildPanel().getSettleBtn().addActionListener(this);
		this.theView.getBuildPanel().getCityBtn().addActionListener(this);
		this.theView.getBuildPanel().getRoadBtn().addActionListener(this);
		for (HexButton hexbutton : this.theView.getBoardPanel().getHexagons()) {
			hexbutton.addActionListener(this);
			hexbutton.setEnabled(false);
		}
		for (Corner corner : this.theView.getBoardPanel().getCorners()) {
			corner.addActionListener(this);
		}
		for (Road road : this.theView.getBoardPanel().getRoads()) {
			road.addActionListener(this);
		}
		for (Port port : this.theView.getBoardPanel().getPorts()) {
			port.addActionListener(this);
		}
	}

	/**
	 * tradingDisable is called after the player clicks trade and disable all of
	 * the buttons except the ones the player
	 */
	public void tradingDisable() {
		this.theView.getBuildPanel().getDevBtn().setEnabled(false);
		this.theView.getBuildPanel().getSettleBtn().setEnabled(false);
		this.theView.getBuildPanel().getCityBtn().setEnabled(false);
		this.theView.getBuildPanel().getRoadBtn().setEnabled(false);
		this.theView.getOptionsPanel().getTradeBtn().setEnabled(false);
		this.theView.getOptionsPanel().getEndTurnBtn().setEnabled(false);
	}

	/**
	 * This method constructs the tiles necessary for resource distribution
	 * 
	 * @return an ArrayList of tiles which hold the current positions and
	 *         resources on the hexagons which are used for distributing
	 *         resources
	 */
	public ArrayList<Tile> makeTiles() {
		ArrayList<Tile> myTiles = new ArrayList<Tile>();
		for (HexButton hexButton : theView.getBoardPanel().getHexagons()) {
			if (hexButton.isVisible())
				myTiles.add(new Tile(hexButton));
		}
		return myTiles;
	}

	/**
	 * startGameDistribute distributes the resources to each player he or she
	 * gets from the tiles surrounding the spots the player settles on
	 */
	public void startGameDistribute() {
		for (Player player : this.theBoard.getPlayers()) {
			for (JButton corner : player.getMyButtons()) {
				if (corner instanceof Corner) {
					int myX = corner.getX();
					int myY = corner.getY();
					for (Tile tile : tiles) {
						if (tile.getPosition()[0] < myX + 3 * hexWidth / 5
								&& tile.getPosition()[0] > myX - 3 * hexWidth
										/ 5
								&& tile.getPosition()[1] < myY + 3 * hexHeight
										/ 5
								&& tile.getPosition()[1] > myY - 3 * hexHeight
										/ 5) {
							if (tile.getMyResource() != null) {
								player.addResource(tile.getMyResource());
							}
						}
					}
				}
			}
		}
	}

	/**
	 * distributeResource distributes resources to the players who settled on a
	 * tile whose number was rolled
	 * 
	 * @param numRolled
	 *            the number the player rolled with the dice
	 */
	public void distributeResource(int numRolled) {
		for (Tile tile : this.tiles) {
			if (tile.getNumber() == numRolled
					&& !tile.getMyButton().hasRobber()) {
				for (Corner corner : this.cornersTaken) {
					if (corner.getX() < tile.getPosition()[0] + 3 * hexWidth
							/ 4
							&& corner.getX() > tile.getPosition()[0] - 3
									* hexWidth / 4
							&& corner.getY() < tile.getPosition()[1] + 3
									* hexHeight / 4
							&& corner.getY() > tile.getPosition()[1] - 3
									* hexHeight / 4) {
						for (Player player : this.theBoard.getPlayers()) {
							if (corner.getColor().equals(player.getMyColor())) {
								player.addResource(tile.getMyResource());
								if (!corner.isSettlement())
									player.addResource(tile.getMyResource());
							}
						}
					}
				}
			}
		}
	}

	/**
	 * This method updates the counts of the cards in the current player's hand
	 */
	public void updateHandPanel() {
		this.theView.getHandPanel().setBrickCount(
				this.theBoard.getCurrentPlayer().getMyHand()
						.get(Card.BRICK.ordinal()));
		this.theView.getHandPanel().setSheepCount(
				this.theBoard.getCurrentPlayer().getMyHand()
						.get(Card.SHEEP.ordinal()));
		this.theView.getHandPanel().setWheatCount(
				this.theBoard.getCurrentPlayer().getMyHand()
						.get(Card.WHEAT.ordinal()));
		this.theView.getHandPanel().setOreCount(
				this.theBoard.getCurrentPlayer().getMyHand()
						.get(Card.ORE.ordinal()));
		this.theView.getHandPanel().setWoodCount(
				this.theBoard.getCurrentPlayer().getMyHand()
						.get(Card.WOOD.ordinal()));
	}

	/**
	 * This method updates the statistics kept for each player
	 */
	public void updatePlayerPanel() {
		for (Player player : this.theBoard.getPlayers()) {
			if (this.theView.getPlayerPanel().updateStats(player)) {
				this.theView
						.getOptionsPanel()
						.getInfoLabel()
						.setText(
								"Player "
										+ this.theBoard.getPlayers().indexOf(
												player) + " won!");
				this.theView.setEnabled(false);
			}
		}
	}

	/**
	 * This method updates the player's options based on the resources they have
	 */
	public void updateBuildPanel() {
		if (this.theBoard.getCurrentPlayer().getDevCards().isEmpty() == false) {
			this.theView.getBuildPanel().getSeeDevelopmentCards()
					.setEnabled(true);
		} else {
			this.theView.getBuildPanel().getSeeDevelopmentCards()
					.setEnabled(false);
		}
		if (this.tradingOne || this.tradingTwo) {
			tradingDisable();
		} else {
			if (this.theBoard.getCurrentPlayer().getMyHand()
					.get(Card.ORE.ordinal()) > 2
					&& this.theBoard.getCurrentPlayer().getMyHand()
							.get(Card.WHEAT.ordinal()) > 1
					&& this.theBoard.getCurrentPlayer().getSettlements() > 0
					&& this.theBoard.getCurrentPlayer().getCities() < 4) {
				this.theView.getBuildPanel().getCityBtn().setEnabled(true);
			} else {
				this.theView.getBuildPanel().getCityBtn().setEnabled(false);
			}
			if (this.theBoard.getCurrentPlayer().getMyHand()
					.get(Card.ORE.ordinal()) > 0
					&& this.theBoard.getCurrentPlayer().getMyHand()
							.get(Card.WHEAT.ordinal()) > 0
					&& this.theBoard.getCurrentPlayer().getMyHand()
							.get(Card.SHEEP.ordinal()) > 0) {
				this.theView.getBuildPanel().getDevBtn().setEnabled(true);
			} else {
				this.theView.getBuildPanel().getDevBtn().setEnabled(false);
			}
			if (this.theBoard.getCurrentPlayer().getMyHand()
					.get(Card.SHEEP.ordinal()) > 0
					&& this.theBoard.getCurrentPlayer().getMyHand()
							.get(Card.WHEAT.ordinal()) > 0
					&& this.theBoard.getCurrentPlayer().getMyHand()
							.get(Card.WOOD.ordinal()) > 0
					&& this.theBoard.getCurrentPlayer().getMyHand()
							.get(Card.BRICK.ordinal()) > 0
					&& this.theView.getBoardPanel().canBuildSettlement(
							this.theBoard.getCurrentPlayer().getMyButtons())) {
				this.theView.getBuildPanel().getSettleBtn().setEnabled(true);
			} else {
				this.theView.getBuildPanel().getSettleBtn().setEnabled(false);
			}
			if (this.theBoard.getCurrentPlayer().getMyHand()
					.get(Card.WOOD.ordinal()) > 0
					&& this.theBoard.getCurrentPlayer().getMyHand()
							.get(Card.BRICK.ordinal()) > 0) {
				this.theView.getBuildPanel().getRoadBtn().setEnabled(true);
			} else {
				this.theView.getBuildPanel().getRoadBtn().setEnabled(false);
			}
		}
	}

	/**
	 * This method determines whether a player has enough cards to trade
	 * 
	 * @param cardsRequired
	 *            the cards required to trade
	 * @return true if the player has enough cards to trade
	 */
	public boolean canTrade(int cardsRequired) {
		this.theView.getOptionsPanel().getTradeBtn().setEnabled(false);
		for (Card resource : Card.values()) {
			if (resource.ordinal() > 4)
				break;
			if (this.theBoard.getCurrentPlayer().getMyHand()
					.get(resource.ordinal()) > cardsRequired - 1) {
				return true;
			}
		}
		return false;
	}

	/**
	 * This method enables the ports the current player can use based on the
	 * location of his resources and the resources he has
	 */
	public void enablePorts() {
		for (Port port : this.theView.getBoardPanel().getPorts()) {
			port.setEnabled(false);
			for (JButton corner : this.theBoard.getCurrentPlayer()
					.getMyButtons()) {
				if (corner instanceof Corner) {
					if (corner.getX() < port.getPosition()[0] + 2 * hexWidth
							/ 4
							&& corner.getX() > port.getPosition()[0] - 2
									* hexWidth / 4
							&& corner.getY() < port.getPosition()[1] + 2
									* hexHeight / 4
							&& corner.getY() > port.getPosition()[1] - 2
									* hexHeight / 4) {
						port.setEnabled(canUsePort(port));
					}
				}
			}
		}
	}

	/**
	 * This method determines if the current player has enough cards required to
	 * use the ports.
	 * 
	 * @param port
	 *            the port the current player is on
	 * @return true if the player has enough cards to use the port
	 */
	public boolean canUsePort(Port port) {
		if (port.getResource().ordinal() > 4) {
			return canTrade(3);
		}
		if (this.theBoard.getCurrentPlayer().getMyHand()
				.get(port.getResource().ordinal()) > 1) {
			return true;
		}
		return false;
	}

	/**
	 * This method is called when a port is clicked and highlights the resource
	 * that can be traded at the port.
	 * 
	 * @param portCicked
	 *            the port that was clicked
	 */
	public void portClick(Port portCicked) {
		portCicked.setEnabled(false);
		portTrade = true;
		tradingOne = true;
		if (portCicked.getResource().ordinal() > 4) {
			portTake = 3;
			for (Card resource : Card.values()) {
				if (resource.ordinal() > 4)
					break;
				if (this.theBoard.getCurrentPlayer().getMyHand()
						.get(resource.ordinal()) > 2) {
					this.theView.getHandPanel().getHandPanelButtons()
							.get(resource.ordinal()).setEnabled(true);
				} else {
					this.theView.getHandPanel().getHandPanelButtons()
							.get(resource.ordinal()).setEnabled(false);
				}
			}
		} else {
			portTake = 2;
			for (Card resource : Card.values()) {
				if (resource.ordinal() > 4)
					break;
				this.theView.getHandPanel().getHandPanelButtons()
						.get(resource.ordinal()).setEnabled(false);
			}
			if (this.theBoard.getCurrentPlayer().getMyHand()
					.get(portCicked.getResource().ordinal()) > 1) {
				this.theView.getHandPanel().getHandPanelButtons()
						.get(portCicked.getResource().ordinal())
						.setEnabled(true);
			}
		}
	}

	/**
	 * This method is called when a seven is rolled and disables all of the
	 * buttons except the hexagons
	 */
	public void disable() {
		this.theView.getOptionsPanel().getTradeBtn().setEnabled(false);
		this.theView.getOptionsPanel().getEndTurnBtn().setEnabled(false);
		this.theView.getBuildPanel().getSeeDevelopmentCards().setEnabled(false);
		this.theView.getBuildPanel().getDevBtn().setEnabled(false);
		this.theView.getBuildPanel().getSettleBtn().setEnabled(false);
		this.theView.getBuildPanel().getCityBtn().setEnabled(false);
		this.theView.getBuildPanel().getRoadBtn().setEnabled(false);
		for (Port port : this.theView.getBoardPanel().getPorts()) {
			port.setEnabled(false);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (setUp) {
			// don't let the player end turn while in setup mode
			this.theView.getOptionsPanel().getEndTurnBtn().setEnabled(false);
			for (Corner corner : this.theView.getBoardPanel().getCorners()) {
				if (e.getSource().equals(corner)) {
					this.theView.getOptionsPanel().getInfoLabel()
							.setText("Build a road");
					corner.setEnabled(false);
					corner.setColor(this.theBoard.getCurrentPlayer()
							.getMyColor());
					this.theBoard.getCurrentPlayer().addOwnedButtons(corner);
					this.theView.getBoardPanel().showAvailableCorners(corner);
					this.theView.getBoardPanel().buildRoad(corner);
					this.theView.getBoardPanel().hideCorners();
					this.cornersTaken.add(corner);
					break;
				}
			}
			for (Road road : this.theView.getBoardPanel().getRoads()) {
				if (e.getSource().equals(road)) {
					road.setEnabled(false);
					this.theBoard.getCurrentPlayer().addOwnedButtons(road);
					this.theView.getBoardPanel().getRoads().remove(road);
					road.setBackground(this.theBoard.getCurrentPlayer()
							.getMyColor());
					this.theView.getBoardPanel().hideRoads();
					this.theView.getBoardPanel().showCorners();
					if (startUpNum < this.theBoard.getPlayers().size() * 2 - 1) {
						this.theView.getOptionsPanel().getInfoLabel()
								.setText("Place a settlement");
						this.startUpNum++;
						if (this.theBoard.getPlayers()
								.get(this.theBoard.getPlayers().size() - 1)
								.getMyButtons().size() > 0)
							this.theBoard.setCurrentPlayer(this.theBoard
									.getPlayers().size() * 2 - 1 - startUpNum);
						else {
							this.theBoard.setCurrentPlayer(startUpNum);
						}
					}
					break;
				}
			}
			if (this.theBoard.getCurrentPlayer().equals(
					this.theBoard.getPlayers().get(0))
					&& this.theBoard.getCurrentPlayer().getMyButtons().size() == 4) {
				this.theView.getBoardPanel().hideCorners();
				this.theView.getBoardPanel().hideRoads();
				setUp = false;
				this.enablePorts();
				this.startGameDistribute();
				int num = SettlersUtility.rollDie();
				this.distributeResource(num);
				this.theView.getOptionsPanel().getInfoLabel()
						.setText("You just rolled a " + num);
				this.theView.getOptionsPanel().getEndTurnBtn().setEnabled(true);
				if (num == 7) {
					this.theView.getOptionsPanel().getEndTurnBtn()
							.setEnabled(false);
					this.theView.getOptionsPanel().getInfoLabel()
							.setText("You just rolled a 7, move the robber");
					this.disable = true;
					for (HexButton hexbutton : this.theView.getBoardPanel()
							.getHexagons()) {
						if (!hexbutton.equals(this.theView.getBoardPanel()
								.getClicked()))
							hexbutton.setEnabled(true);
					}
					for (Player player : this.theBoard.getPlayers()) {
						player.discardCards();
					}
				}
			}

		} else {
			if (e.getSource() instanceof Port) {
				this.theView.getOptionsPanel().getInfoLabel()
						.setText("Select the resource to trade");
				portClick((Port) e.getSource());
			}

			else if (e.getSource().equals(
					this.theView.getBuildPanel().getDevBtn())) {
				this.theBoard.getCurrentPlayer().buyDevelopmentCard();

			} else if (e.getSource().equals(
					this.theView.getBuildPanel().getSeeDevelopmentCards())) {
				DevelopmentCardPopUp d = new DevelopmentCardPopUp(
						this.theBoard.getCurrentPlayer());
				DevelopmentCardController c = new DevelopmentCardController(
						this.theBoard.getCurrentPlayer(), d, this);
				d.setVisible(true);
			}

			else if (e.getSource().equals(
					this.theView.getBuildPanel().getRoadBtn())) {
				this.theView.getOptionsPanel().getInfoLabel()
						.setText("Select a road");
				this.theView.getBoardPanel().buildRoad(
						this.theBoard.getCurrentPlayer().getMyButtons());
				this.theBoard.getCurrentPlayer().buildRoad();
			}

			else if (e.getSource().equals(
					this.theView.getBuildPanel().getSettleBtn())) {
				this.theView.getOptionsPanel().getInfoLabel()
						.setText("Select a settlement");
				this.theView.getBoardPanel().buildSettlement(
						this.theBoard.getCurrentPlayer().getMyButtons());
				this.theBoard.getCurrentPlayer().buildSettlement();
			}

			else if (e.getSource().equals(
					this.theView.getBuildPanel().getCityBtn())) {
				this.theView.getOptionsPanel().getInfoLabel()
						.setText("Select a settlement");
				for (JButton settlement : this.theBoard.getCurrentPlayer()
						.getMyButtons()) {
					if (settlement instanceof Corner) {
						if (((Corner) settlement).isSettlement())
							settlement.setEnabled(true);
					}
				}
				this.theBoard.getCurrentPlayer().buildCity();
			} else if (e.getSource().equals(
					this.theView.getOptionsPanel().getEndTurnBtn())) {
				this.theBoard.nextTurn();
				int num = SettlersUtility.rollDie();
				distributeResource(num);
				this.theView.getOptionsPanel().getInfoLabel()
						.setText("You just rolled a " + num);
				if (num == 7) {
					this.theView.getOptionsPanel().getEndTurnBtn()
							.setEnabled(false);
					this.theView.getOptionsPanel().getInfoLabel()
							.setText("You just rolled a 7, move the robber");
					this.disable = true;
					for (HexButton hexbutton : this.theView.getBoardPanel()
							.getHexagons()) {
						if (!hexbutton.equals(this.theView.getBoardPanel()
								.getClicked()))
							hexbutton.setEnabled(true);
					}
					for (Player player : this.theBoard.getPlayers()) {
						player.discardCards();
					}
				}
			} else if (e.getSource().equals(
					this.theView.getOptionsPanel().getTradeBtn())) {
				this.theView.getOptionsPanel().getInfoLabel()
						.setText("Select the resource to trade");
				for (Card resource : Card.values()) {
					if (resource.ordinal() > 4)
						break;
					if (this.theBoard.getCurrentPlayer().getMyHand()
							.get(resource.ordinal()) > 3) {
						this.theView.getHandPanel().getHandPanelButtons()
								.get(resource.ordinal()).setEnabled(true);
					} else {
						this.theView.getHandPanel().getHandPanelButtons()
								.get(resource.ordinal()).setEnabled(false);
					}
				}
				this.tradingOne = true;
			} else if (tradingOne) {
				this.theView.getOptionsPanel().getInfoLabel()
						.setText("Select the resource you want");
				tradingOne = false;
				tradingTwo = true;
				for (JButton resource : this.theView.getHandPanel()
						.getHandPanelButtons()) {
					resource.setEnabled(true);
				}
				((JButton) e.getSource()).setEnabled(false);
				for (JButton button : this.theView.getHandPanel()
						.getHandPanelButtons()) {
					if (portTrade) {
						if (e.getSource().equals(button)) {
							this.theBoard
									.getCurrentPlayer()
									.getMyHand()
									.set(this.theView.getHandPanel()
											.getHandPanelButtons()
											.indexOf(button),
											this.theBoard
													.getCurrentPlayer()
													.getMyHand()
													.get(this.theView
															.getHandPanel()
															.getHandPanelButtons()
															.indexOf(button))
													- this.portTake);
						}
					} else {
						if (e.getSource().equals(button)) {
							this.theBoard
									.getCurrentPlayer()
									.getMyHand()
									.set(this.theView.getHandPanel()
											.getHandPanelButtons()
											.indexOf(button),
											this.theBoard
													.getCurrentPlayer()
													.getMyHand()
													.get(this.theView
															.getHandPanel()
															.getHandPanelButtons()
															.indexOf(button)) - 4);
						}
					}
				}
			} else if (tradingTwo) {
				tradingTwo = false;
				portTrade = false;
				this.theView.getOptionsPanel().getEndTurnBtn().setEnabled(true);
				for (JButton resource : this.theView.getHandPanel()
						.getHandPanelButtons()) {
					resource.setEnabled(true);
				}
				for (JButton button : this.theView.getHandPanel()
						.getHandPanelButtons()) {
					if (e.getSource().equals(button)) {
						this.theBoard
								.getCurrentPlayer()
								.getMyHand()
								.set(this.theView.getHandPanel()
										.getHandPanelButtons().indexOf(button),
										this.theBoard
												.getCurrentPlayer()
												.getMyHand()
												.get(this.theView
														.getHandPanel()
														.getHandPanelButtons()
														.indexOf(button)) + 1);
					}
				}

			} else if (this.robbing) {
				this.robbing = false;
				this.theView.getOptionsPanel().getEndTurnBtn().setEnabled(true);
				for (Corner corner : this.cornersTaken) {
					corner.setEnabled(false);
					if (e.getSource().equals(corner)) {
						for (Player player : this.theBoard.getPlayers()) {
							if (player.getMyColor().equals(corner.getColor()))
								this.theBoard.getCurrentPlayer().addResource(
										player.removeRandomCard());
						}
					}
				}
				this.theView.getOptionsPanel().getEndTurnBtn().setEnabled(true);
			} else {
				for (Tile tile : this.tiles) {
					if (e.getSource().equals(tile.getMyButton())) {
						for (HexButton hexButton : this.theView.getBoardPanel()
								.getHexagons()) {
							hexButton.setEnabled(false);
						}
						tile.getMyButton().updateColor();
						this.theView.getBoardPanel().getClicked().updateColor();
						this.theView.getBoardPanel().getClicked()
								.setRobber(false);
						this.theView.getBoardPanel().setClciked(
								tile.getMyButton());
						tile.getMyButton().setRobber(true);
						for (Corner corner : this.cornersTaken) {
							if (corner.getX() < tile.getPosition()[0] + 3
									* hexWidth / 4
									&& corner.getX() > tile.getPosition()[0]
											- 3 * hexWidth / 4
									&& corner.getY() < tile.getPosition()[1]
											+ 3 * hexHeight / 4
									&& corner.getY() > tile.getPosition()[1]
											- 3 * hexHeight / 4) {
								for (Player player : this.theBoard.getPlayers()) {
									this.theView.getOptionsPanel()
											.getInfoLabel()
											.setText("Pick who to take from");
									if (corner.getColor().equals(
											player.getMyColor())
											&& !player.equals(this.theBoard
													.getCurrentPlayer())
											&& player.countResourceCards() > 0) {
										corner.setEnabled(true);
										this.robbing = true;
										this.disable = true;
									}
								}
							}
						}
						break;
					}
				}
				for (Road road : this.theView.getBoardPanel().getRoads()) {
					if (e.getSource().equals(road)) {
						road.setEnabled(false);
						road.setColor(this.theBoard.getCurrentPlayer()
								.getMyColor());
						this.theBoard.getCurrentPlayer().addOwnedButtons(road);
						this.theView.getBoardPanel().getRoads().remove(road);
						road.setBackground(this.theBoard.getCurrentPlayer()
								.getMyColor());
						this.theView.getBoardPanel().hideRoads();
						break;
					}
				}

				for (Corner corner : this.cornersTaken) {
					if (e.getSource().equals(corner)) {
						corner.setCity();
						break;
					}
				}
				for (Corner corner : this.theView.getBoardPanel().getCorners()) {
					if (e.getSource().equals(corner)) {
						corner.setEnabled(false);
						corner.setColor(this.theBoard.getCurrentPlayer()
								.getMyColor());
						this.theBoard.getCurrentPlayer()
								.addOwnedButtons(corner);
						this.theView.getBoardPanel().showAvailableCorners(
								corner);
						this.theView.getBoardPanel().hideCorners();
						this.cornersTaken.add(corner);
						break;
					}
				}

			}
		}
		updateHandPanel();
		updatePlayerPanel();
		updateBuildPanel();
		enablePorts();
		if (setUp) {
			this.theView.getOptionsPanel().getEndTurnBtn().setEnabled(false);
		}
		if (canTrade(4) && (!(tradingOne || tradingTwo)))
			this.theView.getOptionsPanel().getTradeBtn().setEnabled(true);
		else {
			this.theView.getOptionsPanel().getTradeBtn().setEnabled(false);
		}
		if (disable) {
			this.disable = false;
			this.disable();
		}
		this.theView.repaint();
	}

	/**
	 * @return the theView
	 */
	public MainView getTheView() {
		return theView;
	}

	/**
	 * @return the theBoard
	 */
	public Board getTheBoard() {
		return theBoard;
	}

	@Override
	public void componentResized(ComponentEvent e) {
		this.tiles = this.makeTiles();
		for (Port port : this.theView.getBoardPanel().getPorts()) {
			port.setPosition(new int[] { port.getX(), port.getY() });
			port.setBounds(port.getPosition()[0], port.getPosition()[1],
					port.getWidth(), port.getHeight());
		}
	}

	@Override
	public void componentMoved(ComponentEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void componentShown(ComponentEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void componentHidden(ComponentEvent e) {
		// TODO Auto-generated method stub

	}

}
