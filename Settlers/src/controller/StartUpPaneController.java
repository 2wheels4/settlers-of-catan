/**
 * CSCI 205 - Software Design and Engineering
 * Name(s): Cole Conte
 * 
 * Work:	Settlers
 * Created:	Dec 4, 2014
 */
package controller;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.lang.reflect.Field;

import model.Board;
import model.Player;
import theView.MainView;
import theView.StartUpPane;

/**
 * @author coleconte
 *
 */
public class StartUpPaneController implements ActionListener {

	/**
	 * board is the board we are about to use
	 */
	private Board board;
	/**
	 * startUpPane is the pane we are controlling
	 */
	private StartUpPane startUpPane;

	/**
	 * constructor for this controller
	 * 
	 * @param b
	 *            is the board we are about to use
	 * @param s
	 *            is the pane we are controlling
	 */
	public StartUpPaneController(Board b, StartUpPane s) {
		this.board = b;
		this.startUpPane = s;
		this.startUpPane.getStartGame().addActionListener(this);
		this.startUpPane.getAddPlayer().addActionListener(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(this.startUpPane.getStartGame())) {
			if (this.startUpPane.getPlayer4().isVisible()) {
				if (!this.startUpPane.getColor1().equals(
						this.startUpPane.getColor2())
						&& !this.startUpPane
								.getColor1()
								.getSelectedItem()
								.equals(this.startUpPane.getColor3()
										.getSelectedItem())
						&& !this.startUpPane
								.getColor1()
								.getSelectedItem()
								.equals(this.startUpPane.getColor4()
										.getSelectedItem())
						&& !this.startUpPane
								.getColor2()
								.getSelectedItem()
								.equals(this.startUpPane.getColor3()
										.getSelectedItem())
						&& !this.startUpPane
								.getColor2()
								.getSelectedItem()
								.equals(this.startUpPane.getColor4()
										.getSelectedItem())
						&& !this.startUpPane
								.getColor3()
								.getSelectedItem()
								.equals(this.startUpPane.getColor4()
										.getSelectedItem())) {
					Color color = null;
					Field field = null;
					try {
						field = Class.forName("java.awt.Color").getField(
								this.startUpPane.getColor1().getSelectedItem()
										.toString());
					} catch (NoSuchFieldException | SecurityException
							| ClassNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					try {
						color = (Color) field.get(null);
					} catch (IllegalArgumentException | IllegalAccessException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					this.board.addPlayer(new Player(board, color));
					try {
						field = Class.forName("java.awt.Color").getField(
								this.startUpPane.getColor2().getSelectedItem()
										.toString());
					} catch (NoSuchFieldException | SecurityException
							| ClassNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					try {
						color = (Color) field.get(null);
					} catch (IllegalArgumentException | IllegalAccessException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					this.board.addPlayer(new Player(board, color));
					try {
						field = Class.forName("java.awt.Color").getField(
								this.startUpPane.getColor3().getSelectedItem()
										.toString());
					} catch (NoSuchFieldException | SecurityException
							| ClassNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					try {
						color = (Color) field.get(null);
					} catch (IllegalArgumentException | IllegalAccessException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					this.board.addPlayer(new Player(board, color));
					try {
						field = Class.forName("java.awt.Color").getField(
								this.startUpPane.getColor4().getSelectedItem()
										.toString());
					} catch (NoSuchFieldException | SecurityException
							| ClassNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					try {
						color = (Color) field.get(null);
					} catch (IllegalArgumentException | IllegalAccessException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					this.board.addPlayer(new Player(board, color));
					this.startUpPane.dispose();
					MainView theView = null;
					try {
						theView = new MainView(this.board);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					theView.getContentPane().setBackground(Color.CYAN);

					SettlersController controller = new SettlersController(
							theView, this.board);
					theView.setVisible(true);
					theView.setResizable(false);
				} else {
					this.startUpPane.getCantStart().setVisible(true);
				}

			} else {
				if (!this.startUpPane.getColor1().getSelectedItem()
						.equals(this.startUpPane.getColor2().getSelectedItem())
						&& !this.startUpPane
								.getColor1()
								.getSelectedItem()
								.equals(this.startUpPane.getColor3()
										.getSelectedItem())
						&& !this.startUpPane
								.getColor2()
								.getSelectedItem()
								.equals(this.startUpPane.getColor3()
										.getSelectedItem())) {
					Color color = null;
					Field field = null;
					try {
						field = Class.forName("java.awt.Color").getField(
								this.startUpPane.getColor1().getSelectedItem()
										.toString());
					} catch (NoSuchFieldException | SecurityException
							| ClassNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					try {
						color = (Color) field.get(null);
					} catch (IllegalArgumentException | IllegalAccessException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					this.board.addPlayer(new Player(board, color));
					try {
						field = Class.forName("java.awt.Color").getField(
								this.startUpPane.getColor2().getSelectedItem()
										.toString());
					} catch (NoSuchFieldException | SecurityException
							| ClassNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					try {
						color = (Color) field.get(null);
					} catch (IllegalArgumentException | IllegalAccessException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					this.board.addPlayer(new Player(board, color));
					try {
						field = Class.forName("java.awt.Color").getField(
								this.startUpPane.getColor3().getSelectedItem()
										.toString());
					} catch (NoSuchFieldException | SecurityException
							| ClassNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					try {
						color = (Color) field.get(null);
					} catch (IllegalArgumentException | IllegalAccessException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					this.board.addPlayer(new Player(board, color));
					this.startUpPane.dispose();
					MainView theView = null;
					try {
						theView = new MainView(board);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					theView.getContentPane().setBackground(Color.CYAN);

					SettlersController controller = new SettlersController(
							theView, board);
					theView.setVisible(true);

				} else {
					this.startUpPane.getCantStart().setVisible(true);
				}
			}
		} else if (e.getSource().equals(this.startUpPane.getAddPlayer())) {
			if (!this.startUpPane.getPlayer4().isVisible()) {
				this.startUpPane.getPlayer4().setVisible(true);
				this.startUpPane.getColor4().setVisible(true);
				this.startUpPane.getAddPlayer().setText("Remove Player");
			} else {
				this.startUpPane.getPlayer4().setVisible(false);
				this.startUpPane.getColor4().setVisible(false);
				this.startUpPane.getAddPlayer().setText("Add Player");
			}
		}

	}
}
