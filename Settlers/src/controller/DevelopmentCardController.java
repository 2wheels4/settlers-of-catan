/**
 * CSCI 205 - Software Design and Engineering
 * Name(s): Cole Conte
 * 
 * Work:	Settlers
 * Created:	Dec 8, 2014
 */
package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import model.Card;
import model.Player;
import theView.DevelopmentCardPopUp;
import theView.HexButton;

/**
 * @author coleconte
 *
 */
public class DevelopmentCardController implements ActionListener {

	/**
	 * player is the player we are using a dev card with
	 */
	private Player player;
	/**
	 * popup is the DevelopmentCardPopUp we are controlling
	 */
	private DevelopmentCardPopUp popUp;
	/**
	 * mainController is the main screen's controller
	 */
	private SettlersController mainController;

	/**
	 * constructor
	 * 
	 * @param p
	 *            the current player
	 * @param d
	 *            the pop up view
	 * @param c
	 *            controller of the main screen
	 */
	public DevelopmentCardController(Player p, DevelopmentCardPopUp d,
			SettlersController c) {
		this.player = p;
		this.popUp = d;
		this.mainController = c;

		this.popUp.getCancel().addActionListener(this);
		this.popUp.getChapel().addActionListener(this);
		this.popUp.getGovernorsHouse().addActionListener(this);
		this.popUp.getLibrary().addActionListener(this);
		this.popUp.getMarket().addActionListener(this);
		this.popUp.getMonopoly().addActionListener(this);
		this.popUp.getRoadBuilding().addActionListener(this);
		this.popUp.getSoldier().addActionListener(this);
		this.popUp.getUniversityOfCatan().addActionListener(this);
		this.popUp.getYearOfPlenty().addActionListener(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(this.popUp.getChapel())) {
			this.player.addVP();
			JButton b = (JButton) e.getSource();
			b.setEnabled(false);
			this.mainController.getTheBoard().getCurrentPlayer()
					.removeDevcard(Card.CHAPEL);
		} else if (e.getSource().equals(this.popUp.getGovernorsHouse())) {
			this.player.addVP();
			JButton b = (JButton) e.getSource();
			b.setEnabled(false);
			this.mainController.getTheBoard().getCurrentPlayer()
					.removeDevcard(Card.GOVERNORSHOUSE);
		} else if (e.getSource().equals(this.popUp.getLibrary())) {
			this.player.addVP();
			JButton b = (JButton) e.getSource();
			b.setEnabled(false);
			this.mainController.getTheBoard().getCurrentPlayer()
					.removeDevcard(Card.LIBRARY);
		} else if (e.getSource().equals(this.popUp.getMarket())) {
			this.player.addVP();
			JButton b = (JButton) e.getSource();
			b.setEnabled(false);
			this.mainController.getTheBoard().getCurrentPlayer()
					.removeDevcard(Card.MARKET);
		} else if (e.getSource().equals(this.popUp.getUniversityOfCatan())) {
			this.player.addVP();
			JButton b = (JButton) e.getSource();
			b.setEnabled(false);
			this.mainController.getTheBoard().getCurrentPlayer()
					.removeDevcard(Card.UNIVERSITYOFCATAN);
		} else if (e.getSource().equals(this.popUp.getYearOfPlenty())) {

		} else if (e.getSource().equals(this.popUp.getMonopoly())) {

		} else if (e.getSource().equals(this.popUp.getRoadBuilding())) {

		} else if (e.getSource().equals(this.popUp.getSoldier())) {
			this.player.incKnights();
			JButton b = (JButton) e.getSource();
			b.setEnabled(false);
			this.mainController.getTheBoard().getCurrentPlayer()
					.removeDevcard(Card.SOLDIER);
			this.popUp.dispose();
			this.mainController.getTheView().getOptionsPanel().getInfoLabel()
					.setText("You played a knight card, move the robber");
			this.mainController.disable();
			for (HexButton hexbutton : this.mainController.getTheView()
					.getBoardPanel().getHexagons()) {
				if (!hexbutton.equals(this.mainController.getTheView()
						.getBoardPanel().getClicked()))
					hexbutton.setEnabled(true);
			}

		} else if (e.getSource().equals(this.popUp.getCancel())) {
			this.popUp.dispose();
		}

	}

}
