import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;

import model.Board;
import model.Player;
import theView.PlayerPanel;

/**
 * CSCI 205 - Software Design and Engineering
 * Name(s): Cole Conte
 * 
 * Work:	FinalProject
 * Created:	Nov 19, 2014
 */

/**
 * @author coleconte
 *
 */
public class testPlayerPanel {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					JFrame frame = new JFrame();
					Board b = new Board();
					Player p = new Player(b, Color.RED);
					Player p2 = new Player(b, Color.BLUE);
					b.addPlayer(p);
					b.addPlayer(p2);
					PlayerPanel pp = new PlayerPanel(b);
					frame.add(pp);
					frame.setVisible(true);
					frame.pack();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

}
